# WacApp

Application for scheduling of hospital appointments and managment of patient health cards.

Repository: [https://gitlab.com/baxxos/wac-app](https://gitlab.com/baxxos/wac-app)

Azure Project: [https://dev.azure.com/meluchm/WAC%202019](https://dev.azure.com/meluchm/WAC%202019)

> The repository in Azure is a push mirror of the GitLab repository. No commits should be made directly to the mirror.

## Authors

- Michal Melúch
- Miloslav Smetana
- Peter Písecký

## Links to Developed Product
App: [https://wac-2019-project-fe.azurewebsites.net/](https://wac-2019-project-fe.azurewebsites.net/)

Services:
 - [Hospital Catalogue Service](https://wac-hospital-catalogue.azurewebsites.net/swagger/index.html)
 - [Appointments Scheduler Service](https://wac-appointments-scheduler.azurewebsites.net/swagger/index.html)
 - [Health Cards Service](https://wac-health-cards.azurewebsites.net/swagger/index.html)


# Documentation

## Use cases

### Appointments Scheduling

> Ako pacient chcem mať prehľad o lekároch (resp. ambulanciách, ku ktorým sa môžem objednať v danom dátume na voľný termín

### Treatment Tracking

> Ako pacient chcem mať prehľad o svojich chorobách, ako aj o závažnosti chorôb a sposobe liečenia (napr. predpísané lieky)

### Patient Managment

> Ako lekár chcem mať prehľad o uskutočnených a plánovanych návštevách (príp. ich aj rušiť) a chcem mat možnosť upravovať zdravotný stav v pacientovom profile.

## Authentication

Users of the developed product have to be signed in in order to use it. Providing a correct username is sufficient. Available accounts are listed below.

### Patients

- turing
- musk

### Doctors

- healer
- tracer
- supportman
- kneewhacker

## API

`.yaml` definitions for all microservices can be found in the `defs/API` directory. Readable markdown versions can be found at:

- [Hospital Catalogue Service](docs/API/hospital-catalogue-api-def.md) 
- [Appointments Scheduler Service](docs/API/appointments-scheduler-api-def.md) 
- [Health Cards Service](docs/API/health-cards-api-def.md) 

## Continuous Integration and Continuous Deployment

Configuration files for CI and CD pipelines can be found in the `defs/CI` and `defs/CD` directories respectively. Due to the fact that Azure currently doesn't seem to provide a way to export CD pipelines as `.yaml` only the `.json` version is provided. Readable markdown versions of available `.yaml` files can be found in:

- [Frontend build tasks](docs/CI/CI-frontend-build-tasks.md)
- [Microservices build tasks](docs/CI/CI-microservices-build-tasks.md)

CI has been set to only trigger the execution of the appropriate build pipeline upon receiving commits to the `backend` or `frontend` directories.

CD triggers after every new build. Only one deployment is ever run at one time and subsequent releases cancel others that are in progress.

## Data Model

During the design phase we have determined that the appropriate data model from a global perspective (i.e. across all services) is the one pictured below:

![Data model](docs/WAC_data_model.png)


## Initial GUI mockups

During the product design phase we have created a several mockups of the user interface which can be found [here](docs/GUI/mockups.md).