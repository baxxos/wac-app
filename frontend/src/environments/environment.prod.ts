export const environment = {
  production: true,
  services: {
    hospitalCatalogue: 'https://wac-hospital-catalogue.azurewebsites.net',
    appointmentsScheduler: 'https://wac-appointments-scheduler.azurewebsites.net',
    healthCards: 'https://wac-health-cards.azurewebsites.net'
  }
};
