import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { State, selectAmbulance, selectAuth } from '@app/store';
import { Observable, of, EMPTY } from 'rxjs';
import { Ambulance } from '@store/ambulance/ambulance.model';
import { AppointmentSchedulerService } from '@services/appointment-scheduler.service';
import * as fromAmbulance from '@store/ambulance/ambulance.reducer';
import { AmbulanceSchedule } from '@store/ambulance-schedule/ambulance-schedule.model';
import { MatDialog } from '@angular/material/dialog';
import { NewAppointmentModalComponent } from './new-appointment-modal/new-appointment-modal.component';
import { Shift } from '@store/shift/shift.model';
import { HospitalCatalogueService } from '@services/hospital-catalogue.service';
import { AddShift, ShiftActionTypes } from '@app/store/shift/shift.actions';
import { tap, map, mergeMap, catchError } from 'rxjs/operators';
import { User } from '@app/store/user/user.model';
import { Appointment } from '@app/store/appointment/appointment.model';
import { CancelAppointmentModalComponent } from '../cancel-appointment-modal/cancel-appointment-modal.component';


@Component({
  selector: 'app-appointment-scheduler',
  templateUrl: './appointment-scheduler.component.html',
  styleUrls: ['./appointment-scheduler.component.scss']
})

export class AppointmentSchedulerComponent implements OnInit {

  public ambulances$: Observable<Ambulance[]>;
  public ambulanceSchedule$: Observable<Appointment[]>;
  public patientSchedule$: Observable<Appointment[]>;
  public shiftDoctor$: Observable<User>;
  public selectedAmbulance: Ambulance;
  public displayedAmbulanceColumns: string[] = ['id', 'name', 'actions'];
  public ambulanceScheduleColumns: string[] = ['timeStart', 'timeEnd', 'available', 'actions'];
  public minAppointmentDate: Date = new Date();
  public now: Date = new Date();
  public selectedDate;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly store: Store<State>,
    private readonly appointmentSchedulerService: AppointmentSchedulerService,
    private readonly hospitalCatalogueService: HospitalCatalogueService,
    public dialog: MatDialog
  ) {
    this.ambulances$ = store.pipe(
      select(selectAmbulance),
      select(fromAmbulance.adapter.getSelectors().selectAll)
    );

    setInterval(() => {
      this.now = new Date();
    }, 1000);

    if (this.minAppointmentDate.getHours() >= 18) {
      this.minAppointmentDate.setDate(this.minAppointmentDate.getDate() + 1)
    }

  }

  /**
   * View ambulance schedule event handler
   *
   * @param ambulance
   */
  public onViewAmbulanceSchedule(ambulance: Ambulance) {
    this.selectedAmbulance = ambulance;

    if (!this.selectedDate) {
      this.selectedDate = this.minAppointmentDate;
    }

    this.onDateSelect(this.selectedDate);
  }

  /**
   * Request appointment button click handler
   *
   * @param ambulance
   * @param selectedSchedule
   */
  public onRequestAppointment(ambulance: Ambulance, appointmentPlaceholder: Appointment) {
    let dialogRef = this.dialog.open(NewAppointmentModalComponent, {
      data: {
        ambulance,
        appointment: appointmentPlaceholder
      }
    });

    dialogRef.afterClosed().subscribe((success: boolean) => success && this.onDateSelect(this.selectedDate))
  }



  /**
   * Datepicker available dates filter
   */
  public weekDaysFilter(date: Date): boolean {
    const day = date.getDay();
    return day !== 0 && day !== 6;  // Prevent Saturday and Sunday from being selected
  }

  /**
   * Date select event handler
   */
  public onDateSelect(date: Date) {
    this.patientSchedule$ = EMPTY;
    this.ambulanceSchedule$ = EMPTY;

    // Mark already reserved appointments as unavailable after a response is received from the server
    this.updateAppointmentPlaceholders(this.selectedAmbulance, date);

    // Display upcoming appointments
    this.loadUpcomingAppointments(this.selectedAmbulance);

    // Set the doctor associated with the currently selected shift
    this.hospitalCatalogueService.getShiftData(this.selectedAmbulance.id, date).pipe(
      mergeMap((shiftData: Shift) => this.hospitalCatalogueService.getUserData(shiftData.id_doctor)),
      catchError(err => this.shiftDoctor$ = EMPTY)
    ).subscribe((user: User) => this.shiftDoctor$ = of(user));
  }

  private getAppointmentPlaceholders(earlierstAppointmentHour: number, appointmentDurationHours: number, numberOfAppointments: number) {
    const appointmentPlaceholders: Appointment[] = [];

    for (let i = 0; i <= numberOfAppointments; i++) {
      const timeStart = new Date(this.selectedDate);
      timeStart.setHours(earlierstAppointmentHour + i); // First appointment starts at 6:00
      timeStart.setMinutes(0, 0, 0);

      const timeEnd = new Date(this.selectedDate);
      timeEnd.setHours(earlierstAppointmentHour + i + appointmentDurationHours);
      timeEnd.setMinutes(0, 0, 0);

      appointmentPlaceholders.push(
        {
          id: null,
          ambulanceId: this.selectedAmbulance.id,
          patientId: null,
          patientDiseaseId: null,
          timeStart,
          timeEnd,
          reserved: false
        }
      );
    }

    return appointmentPlaceholders;
  }

  private updateAppointmentPlaceholders(ambulance: Ambulance, date: Date) {
    const dateFrom = new Date(date);
    dateFrom.setHours(0, 0, 0, 0);

    const dateTo = new Date(date);
    dateTo.setHours(24, 0, 0, 0);

    this.appointmentSchedulerService.getAmbulanceAppointments(ambulance.id, dateFrom, dateTo)
      .subscribe((reservedAppointments: Appointment[]) => {
        // Display appointment placeholders
        const appointmentPlaceholders = this.getAppointmentPlaceholders(6, 1, 11);
        this.ambulanceSchedule$ = of(appointmentPlaceholders);

        // Update the placeholders with data from the server
        this.ambulanceSchedule$.subscribe((previousSchedule: Appointment[]) => {
          for (const appointmentPlaceholder of previousSchedule) {
            for (const reservedAppointment of reservedAppointments) {
              const startsAfterCurrentTimeslotOpens =
                reservedAppointment.timeStart.getUTCHours() >= appointmentPlaceholder.timeStart.getUTCHours() &&
                reservedAppointment.timeStart.getUTCMinutes() >= appointmentPlaceholder.timeStart.getUTCMinutes();

              const endsBeforeCurrentTimeslotCloses =
                reservedAppointment.timeEnd.getUTCHours() <= appointmentPlaceholder.timeEnd.getUTCHours() &&
                reservedAppointment.timeEnd.getUTCMinutes() <= appointmentPlaceholder.timeEnd.getUTCMinutes();

              if (startsAfterCurrentTimeslotOpens && endsBeforeCurrentTimeslotCloses) {
                appointmentPlaceholder.reserved = true;
              }
            }
          }
        });
      });
  }

  private loadUpcomingAppointments(ambulance) {
    const dateFrom = new Date();
    dateFrom.setHours(0, 0, 0, 0);

    const dateTo = new Date(dateFrom);
    dateTo.setHours(24, 0, 0, 0);
    dateTo.setDate(dateTo.getDate() + 30);

    this.store.pipe(
      select(selectAuth),
      mergeMap(authData =>
        this.appointmentSchedulerService.getAmbulanceAppointments(ambulance.id, dateFrom, dateTo, authData.user.id))
    ).subscribe((patientAppointments: Appointment[]) => this.patientSchedule$ = of(patientAppointments));
  }

  ngOnInit() {
    /**
     * Example of fetching ambulance list on init using the service
     *
    let self = this;
    this.appointmentSchedulerService.getAmbulances(1).subscribe(data => {
      self.ambulances$ = of(data);
    });
    */
  }

}
