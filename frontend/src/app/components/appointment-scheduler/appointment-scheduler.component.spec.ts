import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppointmentSchedulerComponent } from './appointment-scheduler.component';
import { RouterTestingModule } from '@angular/router/testing';

import {
  MatIconModule, MatFormFieldModule,
  MatTableModule, MatCardModule, MatDatepickerModule
} from '@angular/material';

import { Store } from '@ngrx/store';
import { empty } from 'rxjs';
import { State } from '@app/store';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { LOCALE_ID } from '@angular/core';
import { AppointmentsTableComponent } from '../appointments-table/appointments-table.component';

const storeStub: Partial<Store<State>> = {
  pipe: () => empty()
};

describe('AppointmentSchedulerComponent', () => {
  let component: AppointmentSchedulerComponent;
  let fixture: ComponentFixture<AppointmentSchedulerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AppointmentsTableComponent, AppointmentSchedulerComponent],
      imports: [
        MatTableModule, MatIconModule, MatCardModule, MatDatepickerModule, MatFormFieldModule, MatIconModule,
        FormsModule,
        RouterTestingModule,
        HttpClientModule
      ],
      providers: [
        { provide: Store, useValue: storeStub },
        { provide: LOCALE_ID, useValue: 'sk-SK' },
        { provide: 'HOSPITAL_CATALOGUE_SERVICE_ENDPOINT', useValue: 'http://dummy.io' },
        { provide: 'APPOINTMENTS_SCHEDULER_SERVICE_ENDPOINT', useValue: 'http://dummy.io' },
        { provide: 'HEALTH_CARD_SERVICE_ENDPOINT', useValue: 'http://dummy.io' }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppointmentSchedulerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
