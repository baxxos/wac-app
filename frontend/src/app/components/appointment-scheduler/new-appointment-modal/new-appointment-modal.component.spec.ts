import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewAppointmentModalComponent } from './new-appointment-modal.component';
import {
  MatFormFieldModule, MatSelectModule, MatIconModule, MatProgressSpinnerModule, MatDialogRef,
  MAT_DIALOG_DATA, MatInputModule
} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { Store } from '@ngrx/store';
import { EMPTY } from 'rxjs';
import { State } from '@app/store';
import { HttpClientModule } from '@angular/common/http';

const storeStub: Partial<Store<State>> = {
  pipe: () => EMPTY
};

describe('NewAppointmentModalComponent', () => {
  let component: NewAppointmentModalComponent;
  let fixture: ComponentFixture<NewAppointmentModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NewAppointmentModalComponent],
      imports: [
        BrowserAnimationsModule,
        FormsModule,
        HttpClientModule,
        MatFormFieldModule,
        MatSelectModule,
        MatIconModule,
        MatProgressSpinnerModule,
        MatInputModule,
      ],
      providers: [
        { provide: Store, useValue: storeStub },
        { provide: MatDialogRef, useValue: {} },
        {
          provide: MAT_DIALOG_DATA, useValue: {
            // Initialize ambulance object
            ambulance: {
              id: 0,
              name: 'Test Ambulance',
              room_identifier: 'A123',
              description: 'Test ambulance description'
            },
            // Initialize appointment object
            appointment: {
              id: 0,
              patientId: 0,
              ambulanceId: 0,
              diseaseId: 0,
              timeStart: new Date(),
              timeEnd: new Date()
            },

          }
        },
        { provide: 'HOSPITAL_CATALOGUE_SERVICE_ENDPOINT', useValue: 'http://dummy.io' },
        { provide: 'APPOINTMENTS_SCHEDULER_SERVICE_ENDPOINT', useValue: 'http://dummy.io' },
        { provide: 'HEALTH_CARD_SERVICE_ENDPOINT', useValue: 'http://dummy.io' }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewAppointmentModalComponent);
    component = fixture.componentInstance;

    // Initialize appointment object
    const timeFrom = new Date();
    const timeTo = new Date(timeFrom);
    timeTo.setHours(timeFrom.getHours() + 1);

    component.data = {
      appointment: {
        id: 0,
        patientId: 0,
        ambulanceId: 0,
        diseaseId: 0,
        timeStart: timeFrom,
        timeEnd: timeTo
      }
    }

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
