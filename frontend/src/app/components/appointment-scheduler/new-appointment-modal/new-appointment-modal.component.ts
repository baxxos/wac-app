import { Component, OnInit, Input, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { MatDialogRef } from '@angular/material/dialog'
import { Ambulance } from '@app/store/ambulance/ambulance.model';
import { Appointment } from '@app/store/appointment/appointment.model';
import { Disease } from '@app/store/disease/disease.model';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { State, selectDisease, selectAuth } from '@app/store';
import * as fromDisease from '@store/disease/disease.reducer';
import { tap, catchError, map, mergeMap } from 'rxjs/operators';
import { AppointmentSchedulerService } from '@app/services/appointment-scheduler.service';
import { HealthCardsService } from '@app/services/health-cards.service';
import { PatientDisease } from '@app/store/patient-disease/patient-disease.model';

@Component({
  selector: 'app-new-appointment-modal',
  templateUrl: './new-appointment-modal.component.html',
  styleUrls: ['./new-appointment-modal.component.scss']
})
export class NewAppointmentModalComponent implements OnInit {

  public appointment: Appointment;
  public ambulance: Ambulance;
  public loading = false;
  public patientsDiseases$: Observable<PatientDisease[]>;
  public requestFailed = false;
  public requestSuccessful = false;

  constructor(
    public dialogRef: MatDialogRef<NewAppointmentModalComponent>,
    private readonly store: Store<State>,
    private readonly appointmentSchedulerService: AppointmentSchedulerService,
    private readonly healthCardsService: HealthCardsService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.appointment = data.appointment;
    this.appointment.id = 0;

    this.ambulance = data.ambulance;

    this.patientsDiseases$ = store
      .pipe(
        select(selectAuth),
        mergeMap(authData => {
          return authData.user ? this.healthCardsService.getUsersHealthCard(authData.user.id) : [];
        })
      );

    this.store.pipe(
      select(selectAuth),
    ).subscribe(authData => this.appointment.patientId = authData.user.id);
  }

  /**
   * Create appointment button handler
   */
  public onClickCreateAppointment() {
    this.loading = true;

    this.appointmentSchedulerService.createAppointment(this.ambulance.id, this.appointment).subscribe(
      response => {
        this.loading = false;
        this.requestFailed = !response.ok
        this.requestSuccessful = response.ok;

        if (this.requestSuccessful) {
          setTimeout(() => {
            this.dialogRef.close(this.requestSuccessful);
          }, 2000);
        }
      },
      error => {
        this.loading = false;
        this.requestFailed = true;
        this.requestSuccessful = false;
      }
    );
  }

  /**
   * On request form cancel click
   */
  public onClickCancel() {
    this.dialogRef.close(this.requestSuccessful)
  }


  ngOnInit() {
  }

}
