import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { State, selectAuth } from '@app/store';
import { HealthCardsService } from '@app/services/health-cards.service';
import { Observable, of } from 'rxjs';
import { PatientDisease } from '@app/store/patient-disease/patient-disease.model';
import { mergeMap, map } from 'rxjs/operators';
import { Appointment } from '@app/store/appointment/appointment.model';
import { AppointmentSchedulerService } from '@app/services/appointment-scheduler.service';

export interface PeriodicElement {
  name: string;
  date_start: string;
  date_modify: string;
}

@Component({
  selector: 'app-treatments-tracker',
  templateUrl: './treatments-tracker.component.html',
  styleUrls: ['./treatments-tracker.component.scss']
})
export class TreatmentsTrackerComponent implements OnInit {

  public patientsDiseases$: Observable<PatientDisease[]>;
  public selectedRecord: PatientDisease;
  public relatedAppointments$: Observable<Appointment[]>;
  displayedColumns: string[] = ['id', 'name', 'created_at', 'updated_at', 'actions'];


  constructor(
    private readonly store: Store<State>,
    private readonly healthCardService: HealthCardsService,
    private readonly appointmentsSchedulerService: AppointmentSchedulerService
  ) { }

  /**
   * On view record click handler
   */
  public onViewHealthCardRecord(record: PatientDisease) {
    this.selectedRecord = record;
    this.refreshAppointments();
  }

  /**
   * Load appointments related to the patient disease record
   */
  public refreshAppointments() {
    this.relatedAppointments$ = this.appointmentsSchedulerService
    .getAppointmentsByPatientDiseaseRecord(this.selectedRecord.id);
  }

  ngOnInit() {
    this.patientsDiseases$ = this.store
      .pipe(
        select(selectAuth),
        mergeMap(authData => {
          return authData.user ? this.healthCardService.getUsersHealthCard(authData.user.id) : [];
        })
      );
  }

}
