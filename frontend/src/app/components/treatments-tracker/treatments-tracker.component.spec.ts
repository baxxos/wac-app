import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TreatmentsTrackerComponent } from './treatments-tracker.component';
import {
  MatInputModule, MatIconModule, MatToolbarModule, MatButtonModule, MatFormFieldModule,
  MatTableModule, MatGridListModule, MatCardModule, MatDatepickerModule, MatNativeDateModule, MatListModule
} from '@angular/material';
import { AppointmentsTableComponent } from '../appointments-table/appointments-table.component';
import { EMPTY } from 'rxjs';
import { State } from '@app/store';
import { Store } from '@ngrx/store';
import { HttpClient } from '@angular/common/http';

const storeStub: Partial<Store<State>> = {
  pipe: () => EMPTY
};

describe('TreatmentsTrackerComponent', () => {
  let component: TreatmentsTrackerComponent;
  let fixture: ComponentFixture<TreatmentsTrackerComponent>;
  let httpClientSpy: { get: jasmine.Spy };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MatCardModule,
        MatIconModule,
        MatTableModule,
        MatListModule
      ],
      declarations: [ AppointmentsTableComponent, TreatmentsTrackerComponent ],
      providers: [
        { provide: HttpClient, useValue: httpClientSpy },
        { provide: Store, useValue: storeStub },
        { provide: 'HOSPITAL_CATALOGUE_SERVICE_ENDPOINT', useValue: 'http://dummy.io' },
        { provide: 'APPOINTMENTS_SCHEDULER_SERVICE_ENDPOINT', useValue: 'http://dummy.io' },
        { provide: 'HEALTH_CARD_SERVICE_ENDPOINT', useValue: 'http://dummy.io' }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreatmentsTrackerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
