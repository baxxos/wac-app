import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { State } from '../../store';
import { Login, AuthActionTypes } from '@store/auth/auth.actions';
import { Observable, of } from 'rxjs';
import { Actions, ofType } from '@ngrx/effects';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})

export class LoginComponent implements OnInit {
  public authInProgress = false;
  public authFailed = false;
  public data$: Observable<{ username: string, password: string }> = of({
    username: null,
    password: null
  });

  constructor(
    private actions$: Actions,
    private readonly store: Store<State>
  ) {
    this.actions$.pipe(
      ofType(AuthActionTypes.LOGIN_FAILURE),
    ).subscribe(error => {
      this.authInProgress = false;
      this.authFailed = true;
    });
  }

  ngOnInit() { }

  public onLogin(usernameOrEmail, password) {

    this.authInProgress = true;

    // Example of triggering a login action before/on the page load
    const payload = {
      usernameOrEmail,
      password
    };

    this.store.dispatch(new Login(payload));
  };
}
