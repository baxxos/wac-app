import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientManagementComponent } from './patient-management.component';
import {
  MatTableModule, MatCardModule, MatIconModule, MatFormFieldModule, MatSelectModule, MatProgressSpinnerModule, MatDialogModule, MAT_DIALOG_DATA, MatDialogRef
} from '@angular/material';
import { Store } from '@ngrx/store';
import { State } from '@app/store';
import { EMPTY } from 'rxjs';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

const storeStub: Partial<Store<State>> = {
  pipe: () => EMPTY
};

describe('PatientManagementComponent', () => {
  let component: PatientManagementComponent;
  let fixture: ComponentFixture<PatientManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        HttpClientModule,
        MatCardModule,
        MatDialogModule,
        MatFormFieldModule,
        MatIconModule,
        MatProgressSpinnerModule,
        MatTableModule,
        MatSelectModule,
      ],
      declarations: [PatientManagementComponent],
      providers: [
        { provide: MatDialogRef, useValue: {} },
        { provide: MAT_DIALOG_DATA, useValue: {} },
        { provide: Store, useValue: storeStub },
        { provide: 'HOSPITAL_CATALOGUE_SERVICE_ENDPOINT', useValue: 'http://dummy.io' },
        { provide: 'APPOINTMENTS_SCHEDULER_SERVICE_ENDPOINT', useValue: 'http://dummy.io' },
        { provide: 'HEALTH_CARD_SERVICE_ENDPOINT', useValue: 'http://dummy.io' }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
