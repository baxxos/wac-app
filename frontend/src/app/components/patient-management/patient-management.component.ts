import { Component, OnInit } from '@angular/core';
import { PatientManagementService } from '@services/patient-management.service';
import { Observable, of } from 'rxjs';
import { delay, map, filter, catchError, mergeMap } from 'rxjs/operators';
import { AmbulanceSchedule } from '@store/ambulance-schedule/ambulance-schedule.model';
import { Appointment } from '@app/store/appointment/appointment.model';
import { Shift } from '@app/store/shift/shift.model';
import { AppointmentSchedulerService } from '@app/services/appointment-scheduler.service';
import { Store, select } from '@ngrx/store';
import { State, selectDisease, selectAuth } from '@app/store';
import { User } from '@app/store/user/user.model';
import { MatDialog } from '@angular/material';
import { CancelAppointmentModalComponent } from '../cancel-appointment-modal/cancel-appointment-modal.component';
import { HospitalCatalogueService } from '@app/services/hospital-catalogue.service';
import { Ambulance } from '@app/store/ambulance/ambulance.model';
import { PatientDisease } from '@app/store/patient-disease/patient-disease.model';
import { HealthCardsService } from '@app/services/health-cards.service';
import { Disease } from '@app/store/disease/disease.model';
import * as fromDisease from '@store/disease/disease.reducer';

@Component({
  selector: 'app-patient-management',
  templateUrl: './patient-management.component.html',
  styleUrls: ['./patient-management.component.scss']
})
export class PatientManagementComponent implements OnInit {

  public patients$: Observable<AmbulanceSchedule[]>
  public displayedColumns: string[] = ['date', 'time-start', 'time-end', 'patient', 'actions', 'cancel'];
  public displayedAppointmentColumns: string[] = ['date', 'time', 'patient'];
  public appointments$: Observable<Appointment[]>
  public appointments: Appointment[]
  public shifts$: Observable<Shift[]>
  private datesArray: Date[]
  public selectedAppointment$: Observable<Appointment>;
  public selectedAmbulance: Ambulance;
  private ambulanceArray: Number[]
  private users: User[]
  public selectedPatientDisease$: Observable<PatientDisease>;
  public diseases$: Observable<Disease[]>;
  public id: number
  public loadingPatientRecord: boolean;

  constructor(
    private readonly store: Store<State>,
    private readonly patientManagementService: PatientManagementService,
    private readonly appointmentSchedulerService: AppointmentSchedulerService,
    private readonly hospitalCatalogueService: HospitalCatalogueService,
    private readonly healthCardsService: HealthCardsService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {

    this.shifts$ = this.patientManagementService.getShifts(parseInt(sessionStorage.getItem('userId')));
    this.shifts$.subscribe(shifts => {
      this.datesArray = shifts.map(obj => obj.date)
      this.ambulanceArray = shifts.map(obj => obj.id_ambulance)
      this.updateAppointments()
    });

    this.diseases$ = this.store.pipe(
      select(selectDisease),
      select(fromDisease.adapter.getSelectors().selectAll),
    );
  }

  /**
   * View appointment schedule event handler
   *
   */

  private updateAppointments() {
    this.appointments$ = this.appointmentSchedulerService.getAppointments();
    this.appointments$.subscribe(appointments => {
      let self = this
      this.appointments = appointments.filter(function (item) {
        for (var key in self.datesArray) {
          let arrayDate = new Date(self.datesArray[key]).toDateString();
          let itemDate = new Date(item.timeStart).toDateString();
          if (arrayDate === itemDate && self.ambulanceArray[key] === item.ambulanceId) {
            return true;
          }
        }
        return false;
      });
    })
  }

  /**
    * View appointment schedule event handler
    *
    * @param appointment
    */
  private getAppointment(appointment: Appointment) {
    this.selectedAppointment$ = of(appointment);
    
    this.loadingPatientRecord = true;
    this.selectedPatientDisease$ = this.healthCardsService
      .getPatientDiseaseRecord(appointment.patientDiseaseId)
      .pipe(
        catchError((e: any, o: Observable<PatientDisease>) => {
          const record: PatientDisease = {
            id: 0,
            patient_id: appointment.patientId,
            disease_id: null,
            description: ''
          };
          return of(record);
        }),
        map(v => {
          this.loadingPatientRecord = false;
          return v;
        })
      )
  }

  /**
   * Save a patients record
   *
   * @param record
   * @param appointment
   */
  public onSavePatientDiseaseRecord(record: PatientDisease, appointment: Appointment) {
    this.healthCardsService.upsertPatientDiseaseRecord(record).subscribe(upsertedRecord => {
      this.selectedPatientDisease$ = of(upsertedRecord);
      if (appointment.patientDiseaseId !== upsertedRecord.id) {
        appointment.patientDiseaseId = upsertedRecord.id;
        this.appointmentSchedulerService.updateAppointment(appointment).subscribe(a => {
          this.selectedAppointment$ = of(a);
          this.updateAppointments();
        })
      }
    })
  }

  /**
   * On disease selection change
   *
   * @param event
   */
  public onDiagnosisChange(event: any, patientId: number) {
    this.selectedPatientDisease$ = this.healthCardsService
      .findPatientDiseaseRecord(patientId, event.value)
      .pipe(
        catchError((e: any, o: Observable<PatientDisease>) => {
          const record: PatientDisease = {
            id: 0,
            patient_id: patientId,
            disease_id: event.value,
            description: ''
          };
          return of(record);
        })
      )
  }

  /**
  * Cancel appointment button click handler
  *
  * @param appointment
  */
  public onCancelAppointment(appointment: Appointment) {
    this.hospitalCatalogueService.getAmbulance(appointment.ambulanceId).subscribe(ambulance => {
      let dialogRef = this.dialog.open(CancelAppointmentModalComponent, {
        data: { ambulance, appointment }
      });

      dialogRef.afterClosed().subscribe((success: boolean) => success && this.updateAppointments())
    });
  }

}
