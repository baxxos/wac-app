import { Component, OnInit, Inject } from '@angular/core';
import { Appointment } from '@app/store/appointment/appointment.model';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AppointmentSchedulerService } from '@app/services/appointment-scheduler.service';
import { Ambulance } from '@app/store/ambulance/ambulance.model';

@Component({
  selector: 'app-cancel-appointment-modal',
  templateUrl: './cancel-appointment-modal.component.html',
  styleUrls: ['./cancel-appointment-modal.component.scss']
})
export class CancelAppointmentModalComponent implements OnInit {
  public appointment: Appointment;
  public ambulance: Ambulance;
  public loading = false;
  public requestFailed = false;
  public requestSuccessful = false;

  constructor(
    public dialogRef: MatDialogRef<CancelAppointmentModalComponent>,
    private readonly appointmentSchedulerService: AppointmentSchedulerService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.appointment = data.appointment;
    this.ambulance = data.ambulance;
  }

  /**
   * Create appointment button handler
   */
  public onClickConfirm() {
    this.loading = true;

    this.appointmentSchedulerService.deleteAppointmentById(this.appointment.id).subscribe(
      response => {
        this.loading = false;
        this.requestFailed = !response.ok;
        this.requestSuccessful = response.ok;

        if (this.requestSuccessful) {
          setTimeout(() => {
            this.dialogRef.close(this.requestSuccessful);
          }, 2000);
        }
      },
      error => {
        this.loading = false;
        this.requestFailed = true;
        this.requestSuccessful = false;
      }
    );
  }

  /**
   * On request form cancel click
   */
  public onClickCancel() {
    this.dialogRef.close(this.requestSuccessful)
  }

  ngOnInit() {
  }
}
