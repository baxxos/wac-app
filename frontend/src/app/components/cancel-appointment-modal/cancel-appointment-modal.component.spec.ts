import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CancelAppointmentModalComponent } from './cancel-appointment-modal.component';
import { MatIconModule, MatProgressSpinnerModule, MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { HttpClientModule } from '@angular/common/http';

describe('CancelAppointmentModalComponent', () => {
  let component: CancelAppointmentModalComponent;
  let fixture: ComponentFixture<CancelAppointmentModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CancelAppointmentModalComponent],
      imports: [
        MatIconModule, MatProgressSpinnerModule, MatDialogModule,
        HttpClientModule
      ],
      providers: [
        { provide: MatDialogRef, useValue: {} },
        {
          provide: MAT_DIALOG_DATA, useValue: {
            // Initialize ambulance object
            ambulance: {
              id: 0,
              name: 'Test Ambulance',
              room_identifier: 'A123',
              description: 'Test ambulance description'
            },
            // Initialize appointment object
            appointment: {
              id: 0,
              patientId: 0,
              ambulanceId: 0,
              diseaseId: 0,
              timeStart: null,
              timeEnd: null
            }
          }
        },
        { provide: 'HOSPITAL_CATALOGUE_SERVICE_ENDPOINT', useValue: 'http://dummy.io' },
        { provide: 'APPOINTMENTS_SCHEDULER_SERVICE_ENDPOINT', useValue: 'http://dummy.io' },
        { provide: 'HEALTH_CARD_SERVICE_ENDPOINT', useValue: 'http://dummy.io' }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelAppointmentModalComponent);
    component = fixture.componentInstance;

    // Initialize appointment object
    const timeStart = new Date();
    component.appointment.timeStart = timeStart;
    
    const timeEnd = new Date(timeStart);
    timeEnd.setHours(timeEnd.getHours() + 1);
    component.appointment.timeEnd = timeEnd;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
