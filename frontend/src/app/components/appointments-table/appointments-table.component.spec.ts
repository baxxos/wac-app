import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AppointmentsTableComponent } from './appointments-table.component';
import { MatTableModule, MatDialogModule } from '@angular/material';

describe('AppointmentsTableComponent', () => {
  let component: AppointmentsTableComponent;
  let fixture: ComponentFixture<AppointmentsTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MatTableModule, MatDialogModule
      ],
      declarations: [AppointmentsTableComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppointmentsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
