import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Ambulance } from '@app/store/ambulance/ambulance.model';
import { Appointment } from '@app/store/appointment/appointment.model';
import { CancelAppointmentModalComponent } from '@app/components/cancel-appointment-modal/cancel-appointment-modal.component';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-appointments-table',
  templateUrl: './appointments-table.component.html',
  styleUrls: ['./appointments-table.component.scss']
})
export class AppointmentsTableComponent implements OnInit {

  @Input()
  public patientSchedule$: Observable<Appointment[]>;
  @Input()
  public ambulance: Ambulance;
  public patientScheduleColumns: string[] = ['date', 'timeStart', 'timeEnd', 'notes', 'actions'];
  public now: Date = new Date();

  @Output()
  appointmentCanceled = new EventEmitter<Appointment>();

  constructor(
    public dialog: MatDialog
  ) { 
    setInterval(() => {
      this.now = new Date();
    }, 1000);
  }

  /**
   * Cancel appointment button click handler
   *
   * @param appointment
   */
  public onCancelAppointment(appointment: Appointment) {
    let dialogRef = this.dialog.open(CancelAppointmentModalComponent, {
      data: { ambulace: this.ambulance, appointment }
    });

    dialogRef.afterClosed().subscribe((success: boolean) => success && this.appointmentCanceled.emit(appointment));
  }

  ngOnInit() {}
}
