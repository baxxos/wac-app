import { defer } from 'rxjs/internal/observable/defer';
import { HttpErrorResponse } from '@angular/common/http';

/**
 * Create async observable that emits-once and completes
 * after a JS engine turn
 */
export function asyncData<T>(data: T) {
    return defer(() => Promise.resolve(data));
}

/**
 *  Create async observable error that errors
 *  after a JS engine turn
 */
export function asyncError<T>(errorObject: any) {
    return defer(() => Promise.reject(errorObject));
}

/**
 * Create a 404 http error
 */
export function make404() {
    return new HttpErrorResponse({
        error: 'test 404 error',
        status: 404, statusText: 'Not Found'
    })
}