import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { Disease } from './disease.model';

export enum DiseaseActionTypes {
  LoadDiseases = '[Disease] Load Diseases',
  AddDisease = '[Disease] Add Disease',
  UpsertDisease = '[Disease] Upsert Disease',
  AddDiseases = '[Disease] Add Diseases',
  UpsertDiseases = '[Disease] Upsert Diseases',
  UpdateDisease = '[Disease] Update Disease',
  UpdateDiseases = '[Disease] Update Diseases',
  DeleteDisease = '[Disease] Delete Disease',
  DeleteDiseases = '[Disease] Delete Diseases',
  ClearDiseases = '[Disease] Clear Diseases'
}

export class LoadDiseases implements Action {
  readonly type = DiseaseActionTypes.LoadDiseases;

  constructor(public payload: { diseases: Disease[] }) {}
}

export class AddDisease implements Action {
  readonly type = DiseaseActionTypes.AddDisease;

  constructor(public payload: { disease: Disease }) {}
}

export class UpsertDisease implements Action {
  readonly type = DiseaseActionTypes.UpsertDisease;

  constructor(public payload: { disease: Disease }) {}
}

export class AddDiseases implements Action {
  readonly type = DiseaseActionTypes.AddDiseases;

  constructor(public payload: { diseases: Disease[] }) {}
}

export class UpsertDiseases implements Action {
  readonly type = DiseaseActionTypes.UpsertDiseases;

  constructor(public payload: { diseases: Disease[] }) {}
}

export class UpdateDisease implements Action {
  readonly type = DiseaseActionTypes.UpdateDisease;

  constructor(public payload: { disease: Update<Disease> }) {}
}

export class UpdateDiseases implements Action {
  readonly type = DiseaseActionTypes.UpdateDiseases;

  constructor(public payload: { diseases: Update<Disease>[] }) {}
}

export class DeleteDisease implements Action {
  readonly type = DiseaseActionTypes.DeleteDisease;

  constructor(public payload: { id: string }) {}
}

export class DeleteDiseases implements Action {
  readonly type = DiseaseActionTypes.DeleteDiseases;

  constructor(public payload: { ids: string[] }) {}
}

export class ClearDiseases implements Action {
  readonly type = DiseaseActionTypes.ClearDiseases;
}

export type DiseaseActions =
 LoadDiseases
 | AddDisease
 | UpsertDisease
 | AddDiseases
 | UpsertDiseases
 | UpdateDisease
 | UpdateDiseases
 | DeleteDisease
 | DeleteDiseases
 | ClearDiseases;
