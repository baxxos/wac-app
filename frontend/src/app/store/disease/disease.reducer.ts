import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { Disease } from './disease.model';
import { DiseaseActions, DiseaseActionTypes } from './disease.actions';

export interface State extends EntityState<Disease> {
  // additional entities state properties
}

export const adapter: EntityAdapter<Disease> = createEntityAdapter<Disease>();

export const initialState: State = adapter.getInitialState({
  // additional entity state properties
});

export function reducer(
  state = initialState,
  action: DiseaseActions
): State {
  switch (action.type) {
    case DiseaseActionTypes.AddDisease: {
      return adapter.addOne(action.payload.disease, state);
    }

    case DiseaseActionTypes.UpsertDisease: {
      return adapter.upsertOne(action.payload.disease, state);
    }

    case DiseaseActionTypes.AddDiseases: {
      return adapter.addMany(action.payload.diseases, state);
    }

    case DiseaseActionTypes.UpsertDiseases: {
      return adapter.upsertMany(action.payload.diseases, state);
    }

    case DiseaseActionTypes.UpdateDisease: {
      return adapter.updateOne(action.payload.disease, state);
    }

    case DiseaseActionTypes.UpdateDiseases: {
      return adapter.updateMany(action.payload.diseases, state);
    }

    case DiseaseActionTypes.DeleteDisease: {
      return adapter.removeOne(action.payload.id, state);
    }

    case DiseaseActionTypes.DeleteDiseases: {
      return adapter.removeMany(action.payload.ids, state);
    }

    case DiseaseActionTypes.LoadDiseases: {
      return adapter.addAll(action.payload.diseases, state);
    }

    case DiseaseActionTypes.ClearDiseases: {
      return adapter.removeAll(state);
    }

    default: {
      return state;
    }
  }
}

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = adapter.getSelectors();
