export interface Disease {
  id: number;
  name: string;
  severity: number;
}
