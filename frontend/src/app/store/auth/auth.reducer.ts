import { User } from '../user/user.model';
import { AuthActions, AuthActionTypes } from './auth.actions';

export interface State {
  isAuthenticated: boolean;
  user: User | null;
  token: string;
  errorMessage: string | null;
}

export const initialState: State = {
  isAuthenticated: false,
  user: null,
  token: null,
  errorMessage: null
};


export function reducer(
  state = initialState,
  action: AuthActions
): State {
  switch (action.type) {
    case AuthActionTypes.LOGIN_SUCCESS: {
      return {
        ...state,
        isAuthenticated: true,
        user: action.payload.user,
        token: action.payload.token,
        errorMessage: null
      };
    }
    case AuthActionTypes.LOGIN_FAILURE: {
      return {
        ...state,
        errorMessage: 'Incorrect e-mail and/or password'
      };
    }
    default: {
      return state;
    }
  }
}
