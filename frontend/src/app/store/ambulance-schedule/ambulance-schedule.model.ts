import {User} from '../user/user.model';

export interface AmbulanceSchedule {
  ambulanceId: number;
  from: Date;
  to: Date;
  reserved: boolean;
  doctor?: User;
  patient?: User;
}
