import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { AmbulanceSchedule } from './ambulance-schedule.model';

export enum AmbulanceScheduleActionTypes {
  LoadAmbulanceSchedules = '[AmbulanceSchedule] Load AmbulanceSchedules',
  AddAmbulanceSchedule = '[AmbulanceSchedule] Add AmbulanceSchedule',
  UpsertAmbulanceSchedule = '[AmbulanceSchedule] Upsert AmbulanceSchedule',
  AddAmbulanceSchedules = '[AmbulanceSchedule] Add AmbulanceSchedules',
  UpsertAmbulanceSchedules = '[AmbulanceSchedule] Upsert AmbulanceSchedules',
  UpdateAmbulanceSchedule = '[AmbulanceSchedule] Update AmbulanceSchedule',
  UpdateAmbulanceSchedules = '[AmbulanceSchedule] Update AmbulanceSchedules',
  DeleteAmbulanceSchedule = '[AmbulanceSchedule] Delete AmbulanceSchedule',
  DeleteAmbulanceSchedules = '[AmbulanceSchedule] Delete AmbulanceSchedules',
  ClearAmbulanceSchedules = '[AmbulanceSchedule] Clear AmbulanceSchedules'
}

export class LoadAmbulanceSchedules implements Action {
  readonly type = AmbulanceScheduleActionTypes.LoadAmbulanceSchedules;

  constructor(public payload: { ambulanceSchedules: AmbulanceSchedule[] }) {}
}

export class AddAmbulanceSchedule implements Action {
  readonly type = AmbulanceScheduleActionTypes.AddAmbulanceSchedule;

  constructor(public payload: { ambulanceSchedule: AmbulanceSchedule }) {}
}

export class UpsertAmbulanceSchedule implements Action {
  readonly type = AmbulanceScheduleActionTypes.UpsertAmbulanceSchedule;

  constructor(public payload: { ambulanceSchedule: AmbulanceSchedule }) {}
}

export class AddAmbulanceSchedules implements Action {
  readonly type = AmbulanceScheduleActionTypes.AddAmbulanceSchedules;

  constructor(public payload: { ambulanceSchedules: AmbulanceSchedule[] }) {}
}

export class UpsertAmbulanceSchedules implements Action {
  readonly type = AmbulanceScheduleActionTypes.UpsertAmbulanceSchedules;

  constructor(public payload: { ambulanceSchedules: AmbulanceSchedule[] }) {}
}

export class UpdateAmbulanceSchedule implements Action {
  readonly type = AmbulanceScheduleActionTypes.UpdateAmbulanceSchedule;

  constructor(public payload: { ambulanceSchedule: Update<AmbulanceSchedule> }) {}
}

export class UpdateAmbulanceSchedules implements Action {
  readonly type = AmbulanceScheduleActionTypes.UpdateAmbulanceSchedules;

  constructor(public payload: { ambulanceSchedules: Update<AmbulanceSchedule>[] }) {}
}

export class DeleteAmbulanceSchedule implements Action {
  readonly type = AmbulanceScheduleActionTypes.DeleteAmbulanceSchedule;

  constructor(public payload: { id: string }) {}
}

export class DeleteAmbulanceSchedules implements Action {
  readonly type = AmbulanceScheduleActionTypes.DeleteAmbulanceSchedules;

  constructor(public payload: { ids: string[] }) {}
}

export class ClearAmbulanceSchedules implements Action {
  readonly type = AmbulanceScheduleActionTypes.ClearAmbulanceSchedules;
}

export type AmbulanceScheduleActions =
 LoadAmbulanceSchedules
 | AddAmbulanceSchedule
 | UpsertAmbulanceSchedule
 | AddAmbulanceSchedules
 | UpsertAmbulanceSchedules
 | UpdateAmbulanceSchedule
 | UpdateAmbulanceSchedules
 | DeleteAmbulanceSchedule
 | DeleteAmbulanceSchedules
 | ClearAmbulanceSchedules;
