import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { AmbulanceSchedule } from './ambulance-schedule.model';
import { AmbulanceScheduleActions, AmbulanceScheduleActionTypes } from './ambulance-schedule.actions';

export interface State extends EntityState<AmbulanceSchedule> {
  // additional entities state properties
}

export const adapter: EntityAdapter<AmbulanceSchedule> = createEntityAdapter<AmbulanceSchedule>();

export const initialState: State = adapter.getInitialState({
  // additional entity state properties
});

export function reducer(
  state = initialState,
  action: AmbulanceScheduleActions
): State {
  switch (action.type) {
    case AmbulanceScheduleActionTypes.AddAmbulanceSchedule: {
      return adapter.addOne(action.payload.ambulanceSchedule, state);
    }

    case AmbulanceScheduleActionTypes.UpsertAmbulanceSchedule: {
      return adapter.upsertOne(action.payload.ambulanceSchedule, state);
    }

    case AmbulanceScheduleActionTypes.AddAmbulanceSchedules: {
      return adapter.addMany(action.payload.ambulanceSchedules, state);
    }

    case AmbulanceScheduleActionTypes.UpsertAmbulanceSchedules: {
      return adapter.upsertMany(action.payload.ambulanceSchedules, state);
    }

    case AmbulanceScheduleActionTypes.UpdateAmbulanceSchedule: {
      return adapter.updateOne(action.payload.ambulanceSchedule, state);
    }

    case AmbulanceScheduleActionTypes.UpdateAmbulanceSchedules: {
      return adapter.updateMany(action.payload.ambulanceSchedules, state);
    }

    case AmbulanceScheduleActionTypes.DeleteAmbulanceSchedule: {
      return adapter.removeOne(action.payload.id, state);
    }

    case AmbulanceScheduleActionTypes.DeleteAmbulanceSchedules: {
      return adapter.removeMany(action.payload.ids, state);
    }

    case AmbulanceScheduleActionTypes.LoadAmbulanceSchedules: {
      return adapter.addAll(action.payload.ambulanceSchedules, state);
    }

    case AmbulanceScheduleActionTypes.ClearAmbulanceSchedules: {
      return adapter.removeAll(state);
    }

    default: {
      return state;
    }
  }
}

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = adapter.getSelectors();
