import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { PatientDisease } from './patient-disease.model';

export enum PatientDiseaseActionTypes {
  LoadPatientDiseases = '[PatientDisease] Load PatientDiseases',
  AddPatientDisease = '[PatientDisease] Add PatientDisease',
  UpsertPatientDisease = '[PatientDisease] Upsert PatientDisease',
  AddPatientDiseases = '[PatientDisease] Add PatientDiseases',
  UpsertPatientDiseases = '[PatientDisease] Upsert PatientDiseases',
  UpdatePatientDisease = '[PatientDisease] Update PatientDisease',
  UpdatePatientDiseases = '[PatientDisease] Update PatientDiseases',
  DeletePatientDisease = '[PatientDisease] Delete PatientDisease',
  DeletePatientDiseases = '[PatientDisease] Delete PatientDiseases',
  ClearPatientDiseases = '[PatientDisease] Clear PatientDiseases'
}

export class LoadPatientDiseases implements Action {
  readonly type = PatientDiseaseActionTypes.LoadPatientDiseases;

  constructor(public payload: { patientDiseases: PatientDisease[] }) {}
}

export class AddPatientDisease implements Action {
  readonly type = PatientDiseaseActionTypes.AddPatientDisease;

  constructor(public payload: { patientDisease: PatientDisease }) {}
}

export class UpsertPatientDisease implements Action {
  readonly type = PatientDiseaseActionTypes.UpsertPatientDisease;

  constructor(public payload: { patientDisease: PatientDisease }) {}
}

export class AddPatientDiseases implements Action {
  readonly type = PatientDiseaseActionTypes.AddPatientDiseases;

  constructor(public payload: { patientDiseases: PatientDisease[] }) {}
}

export class UpsertPatientDiseases implements Action {
  readonly type = PatientDiseaseActionTypes.UpsertPatientDiseases;

  constructor(public payload: { patientDiseases: PatientDisease[] }) {}
}

export class UpdatePatientDisease implements Action {
  readonly type = PatientDiseaseActionTypes.UpdatePatientDisease;

  constructor(public payload: { patientDisease: Update<PatientDisease> }) {}
}

export class UpdatePatientDiseases implements Action {
  readonly type = PatientDiseaseActionTypes.UpdatePatientDiseases;

  constructor(public payload: { patientDiseases: Update<PatientDisease>[] }) {}
}

export class DeletePatientDisease implements Action {
  readonly type = PatientDiseaseActionTypes.DeletePatientDisease;

  constructor(public payload: { id: string }) {}
}

export class DeletePatientDiseases implements Action {
  readonly type = PatientDiseaseActionTypes.DeletePatientDiseases;

  constructor(public payload: { ids: string[] }) {}
}

export class ClearPatientDiseases implements Action {
  readonly type = PatientDiseaseActionTypes.ClearPatientDiseases;
}

export type PatientDiseaseActions =
 LoadPatientDiseases
 | AddPatientDisease
 | UpsertPatientDisease
 | AddPatientDiseases
 | UpsertPatientDiseases
 | UpdatePatientDisease
 | UpdatePatientDiseases
 | DeletePatientDisease
 | DeletePatientDiseases
 | ClearPatientDiseases;
