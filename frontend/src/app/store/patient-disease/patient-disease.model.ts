import { Disease } from '../disease/disease.model';

export interface PatientDisease {
  id?: number;
  patient_id: number;
  disease_id?: number;
  description: string;
  created_at?: Date;
  updated_at?: Date;
  disease?: Disease;
}
