import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { PatientDisease } from './patient-disease.model';
import { PatientDiseaseActions, PatientDiseaseActionTypes } from './patient-disease.actions';

export interface State extends EntityState<PatientDisease> {
  // additional entities state properties
}

export const adapter: EntityAdapter<PatientDisease> = createEntityAdapter<PatientDisease>();

export const initialState: State = adapter.getInitialState({
  // additional entity state properties
});

export function reducer(
  state = initialState,
  action: PatientDiseaseActions
): State {
  switch (action.type) {
    case PatientDiseaseActionTypes.AddPatientDisease: {
      return adapter.addOne(action.payload.patientDisease, state);
    }

    case PatientDiseaseActionTypes.UpsertPatientDisease: {
      return adapter.upsertOne(action.payload.patientDisease, state);
    }

    case PatientDiseaseActionTypes.AddPatientDiseases: {
      return adapter.addMany(action.payload.patientDiseases, state);
    }

    case PatientDiseaseActionTypes.UpsertPatientDiseases: {
      return adapter.upsertMany(action.payload.patientDiseases, state);
    }

    case PatientDiseaseActionTypes.UpdatePatientDisease: {
      return adapter.updateOne(action.payload.patientDisease, state);
    }

    case PatientDiseaseActionTypes.UpdatePatientDiseases: {
      return adapter.updateMany(action.payload.patientDiseases, state);
    }

    case PatientDiseaseActionTypes.DeletePatientDisease: {
      return adapter.removeOne(action.payload.id, state);
    }

    case PatientDiseaseActionTypes.DeletePatientDiseases: {
      return adapter.removeMany(action.payload.ids, state);
    }

    case PatientDiseaseActionTypes.LoadPatientDiseases: {
      return adapter.addAll(action.payload.patientDiseases, state);
    }

    case PatientDiseaseActionTypes.ClearPatientDiseases: {
      return adapter.removeAll(state);
    }

    default: {
      return state;
    }
  }
}

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = adapter.getSelectors();
