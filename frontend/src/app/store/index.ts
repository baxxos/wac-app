import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';
import { routerReducer, RouterReducerState, SerializedRouterStateSnapshot } from '@ngrx/router-store';
import { environment } from '../../environments/environment';
import * as fromAmbulance from './ambulance/ambulance.reducer';
import * as fromAuth from './auth/auth.reducer';
import * as fromDisease from './disease/disease.reducer';
import * as fromAppointment from './appointment/appointment.reducer';
import * as fromShift from './shift/shift.reducer';

export interface State {
  router: RouterReducerState<SerializedRouterStateSnapshot>;
  ambulance: fromAmbulance.State;
  auth: fromAuth.State;
  disease: fromDisease.State;
  appointment: fromAppointment.State;
  shift: fromShift.State;



}

export const reducers: ActionReducerMap<State> = {
  router: routerReducer,
  ambulance: fromAmbulance.reducer,
  auth: fromAuth.reducer,
  disease: fromDisease.reducer,
  appointment: fromAppointment.reducer,
  shift: fromShift.reducer,
};

export const selectAmbulance = (state: State) => state.ambulance;
export const selectDisease = (state: State) => state.disease;
export const selectAuth = (state: State) => state.auth;

export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];
