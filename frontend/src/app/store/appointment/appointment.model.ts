export interface Appointment {
  id: number;
  ambulanceId: number;
  patientId: number;
  patientDiseaseId: number;
  timeStart: Date;
  timeEnd: Date;
  notes?: string;
  reserved?: boolean;
}
