import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { Ambulance } from './ambulance.model';
import { AmbulanceActions, AmbulanceActionTypes } from './ambulance.actions';

export interface State extends EntityState<Ambulance> {
  // additional entities state properties
}

export const adapter: EntityAdapter<Ambulance> = createEntityAdapter<Ambulance>();

export const initialState: State = adapter.getInitialState({
  // additional entity state properties
});

export function reducer(
  state = initialState,
  action: AmbulanceActions
): State {
  switch (action.type) {
    case AmbulanceActionTypes.AddAmbulance: {
      return adapter.addOne(action.payload.ambulance, state);
    }

    case AmbulanceActionTypes.UpsertAmbulance: {
      return adapter.upsertOne(action.payload.ambulance, state);
    }

    case AmbulanceActionTypes.AddAmbulances: {
      return adapter.addMany(action.payload.ambulances, state);
    }

    case AmbulanceActionTypes.UpsertAmbulances: {
      return adapter.upsertMany(action.payload.ambulances, state);
    }

    case AmbulanceActionTypes.UpdateAmbulance: {
      return adapter.updateOne(action.payload.ambulance, state);
    }

    case AmbulanceActionTypes.UpdateAmbulances: {
      return adapter.updateMany(action.payload.ambulances, state);
    }

    case AmbulanceActionTypes.DeleteAmbulance: {
      return adapter.removeOne(action.payload.id, state);
    }

    case AmbulanceActionTypes.DeleteAmbulances: {
      return adapter.removeMany(action.payload.ids, state);
    }

    case AmbulanceActionTypes.LoadAmbulances: {
      return adapter.addAll(action.payload.ambulances, state);
    }

    case AmbulanceActionTypes.ClearAmbulances: {
      return adapter.removeAll(state);
    }

    default: {
      return state;
    }
  }
}

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = adapter.getSelectors();
