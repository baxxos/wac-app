import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { Ambulance } from './ambulance.model';

export enum AmbulanceActionTypes {
  LoadAmbulances = '[Ambulance] Load Ambulances',
  AddAmbulance = '[Ambulance] Add Ambulance',
  UpsertAmbulance = '[Ambulance] Upsert Ambulance',
  AddAmbulances = '[Ambulance] Add Ambulances',
  UpsertAmbulances = '[Ambulance] Upsert Ambulances',
  UpdateAmbulance = '[Ambulance] Update Ambulance',
  UpdateAmbulances = '[Ambulance] Update Ambulances',
  DeleteAmbulance = '[Ambulance] Delete Ambulance',
  DeleteAmbulances = '[Ambulance] Delete Ambulances',
  ClearAmbulances = '[Ambulance] Clear Ambulances'
}

export class LoadAmbulances implements Action {
  readonly type = AmbulanceActionTypes.LoadAmbulances;

  constructor(public payload: { ambulances: Ambulance[] }) {}
}

export class AddAmbulance implements Action {
  readonly type = AmbulanceActionTypes.AddAmbulance;

  constructor(public payload: { ambulance: Ambulance }) {}
}

export class UpsertAmbulance implements Action {
  readonly type = AmbulanceActionTypes.UpsertAmbulance;

  constructor(public payload: { ambulance: Ambulance }) {}
}

export class AddAmbulances implements Action {
  readonly type = AmbulanceActionTypes.AddAmbulances;

  constructor(public payload: { ambulances: Ambulance[] }) {}
}

export class UpsertAmbulances implements Action {
  readonly type = AmbulanceActionTypes.UpsertAmbulances;

  constructor(public payload: { ambulances: Ambulance[] }) {}
}

export class UpdateAmbulance implements Action {
  readonly type = AmbulanceActionTypes.UpdateAmbulance;

  constructor(public payload: { ambulance: Update<Ambulance> }) {}
}

export class UpdateAmbulances implements Action {
  readonly type = AmbulanceActionTypes.UpdateAmbulances;

  constructor(public payload: { ambulances: Update<Ambulance>[] }) {}
}

export class DeleteAmbulance implements Action {
  readonly type = AmbulanceActionTypes.DeleteAmbulance;

  constructor(public payload: { id: string }) {}
}

export class DeleteAmbulances implements Action {
  readonly type = AmbulanceActionTypes.DeleteAmbulances;

  constructor(public payload: { ids: string[] }) {}
}

export class ClearAmbulances implements Action {
  readonly type = AmbulanceActionTypes.ClearAmbulances;
}

export type AmbulanceActions =
 LoadAmbulances
 | AddAmbulance
 | UpsertAmbulance
 | AddAmbulances
 | UpsertAmbulances
 | UpdateAmbulance
 | UpdateAmbulances
 | DeleteAmbulance
 | DeleteAmbulances
 | ClearAmbulances;
