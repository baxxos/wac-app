export interface Ambulance {
  id: number;
  name: string;
  description: string;
  room_identifier: string;
}
