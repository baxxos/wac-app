export interface User {
  id: number;
  type: 'patient' | 'doctor';
  username?: string;
  firstname: string;
  lastname: string;
  email?: string;
}
