export interface Shift {
  id: number;
  id_ambulance: number;
  id_doctor: number;
  date: Date;
}
