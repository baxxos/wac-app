import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { Shift } from './shift.model';

export enum ShiftActionTypes {
  LoadShifts = '[Shift] Load Shifts',
  AddShift = '[Shift] Add Shift',
  UpsertShift = '[Shift] Upsert Shift',
  AddShifts = '[Shift] Add Shifts',
  UpsertShifts = '[Shift] Upsert Shifts',
  UpdateShift = '[Shift] Update Shift',
  UpdateShifts = '[Shift] Update Shifts',
  DeleteShift = '[Shift] Delete Shift',
  DeleteShifts = '[Shift] Delete Shifts',
  ClearShifts = '[Shift] Clear Shifts'
}

export class LoadShifts implements Action {
  readonly type = ShiftActionTypes.LoadShifts;

  constructor(public payload: { shifts: Shift[] }) {}
}

export class AddShift implements Action {
  readonly type = ShiftActionTypes.AddShift;

  constructor(public payload: { shift: Shift }) {}
}

export class UpsertShift implements Action {
  readonly type = ShiftActionTypes.UpsertShift;

  constructor(public payload: { shift: Shift }) {}
}

export class AddShifts implements Action {
  readonly type = ShiftActionTypes.AddShifts;

  constructor(public payload: { shifts: Shift[] }) {}
}

export class UpsertShifts implements Action {
  readonly type = ShiftActionTypes.UpsertShifts;

  constructor(public payload: { shifts: Shift[] }) {}
}

export class UpdateShift implements Action {
  readonly type = ShiftActionTypes.UpdateShift;

  constructor(public payload: { shift: Update<Shift> }) {}
}

export class UpdateShifts implements Action {
  readonly type = ShiftActionTypes.UpdateShifts;

  constructor(public payload: { shifts: Update<Shift>[] }) {}
}

export class DeleteShift implements Action {
  readonly type = ShiftActionTypes.DeleteShift;

  constructor(public payload: { id: string }) {}
}

export class DeleteShifts implements Action {
  readonly type = ShiftActionTypes.DeleteShifts;

  constructor(public payload: { ids: string[] }) {}
}

export class ClearShifts implements Action {
  readonly type = ShiftActionTypes.ClearShifts;
}

export type ShiftActions =
 LoadShifts
 | AddShift
 | UpsertShift
 | AddShifts
 | UpsertShifts
 | UpdateShift
 | UpdateShifts
 | DeleteShift
 | DeleteShifts
 | ClearShifts;
