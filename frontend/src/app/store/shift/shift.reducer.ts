import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { Shift } from './shift.model';
import { ShiftActions, ShiftActionTypes } from './shift.actions';

export interface State extends EntityState<Shift> {
  // additional entities state properties
}

export const adapter: EntityAdapter<Shift> = createEntityAdapter<Shift>();

export const initialState: State = adapter.getInitialState({
  // additional entity state properties
});

export function reducer(
  state = initialState,
  action: ShiftActions
): State {
  switch (action.type) {
    case ShiftActionTypes.AddShift: {
      return adapter.addOne(action.payload.shift, state);
    }

    case ShiftActionTypes.UpsertShift: {
      return adapter.upsertOne(action.payload.shift, state);
    }

    case ShiftActionTypes.AddShifts: {
      return adapter.addMany(action.payload.shifts, state);
    }

    case ShiftActionTypes.UpsertShifts: {
      return adapter.upsertMany(action.payload.shifts, state);
    }

    case ShiftActionTypes.UpdateShift: {
      return adapter.updateOne(action.payload.shift, state);
    }

    case ShiftActionTypes.UpdateShifts: {
      return adapter.updateMany(action.payload.shifts, state);
    }

    case ShiftActionTypes.DeleteShift: {
      return adapter.removeOne(action.payload.id, state);
    }

    case ShiftActionTypes.DeleteShifts: {
      return adapter.removeMany(action.payload.ids, state);
    }

    case ShiftActionTypes.LoadShifts: {
      return adapter.addAll(action.payload.shifts, state);
    }

    case ShiftActionTypes.ClearShifts: {
      return adapter.removeAll(state);
    }

    default: {
      return state;
    }
  }
}

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = adapter.getSelectors();
