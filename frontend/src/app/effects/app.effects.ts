import { Injectable } from '@angular/core';
import { Actions, Effect, ofType, ROOT_EFFECTS_INIT } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { Action } from '@ngrx/store';

import { LoadAmbulances } from '../store/ambulance/ambulance.actions';
import { LoadDiseases } from '../store/disease/disease.actions';

import { mergeMap, map } from 'rxjs/operators';
import { HospitalCatalogueService } from '../services/hospital-catalogue.service';

@Injectable()
export class AppEffects {

  constructor(private actions$: Actions, private hospitalCatalogueService: HospitalCatalogueService) { }


  @Effect()
  initAmbulances$: Observable<Action> = this.actions$.pipe(
    ofType(ROOT_EFFECTS_INIT),
    mergeMap(_ => this.hospitalCatalogueService.getAmbulances()),
    map(ambulances => new LoadAmbulances({ ambulances }))
  );

  @Effect()
  initDiseases$: Observable<Action> = this.actions$.pipe(
    ofType(ROOT_EFFECTS_INIT),
    mergeMap(_ => this.hospitalCatalogueService.getDiseases()),
    map(diseases => new LoadDiseases({ diseases }))
  );

}
