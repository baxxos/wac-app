import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { AuthActionTypes, Login, LoginSuccess, LoginFailure } from '../store/auth/auth.actions';
import { map, switchMap, catchError, tap } from 'rxjs/operators';



@Injectable()
export class AuthEffects {

  constructor(
    private actions$: Actions,
    private authService: AuthService,
    private router: Router,
  ) { }

  @Effect()
  login$: Observable<any> = this.actions$.pipe(
    ofType(AuthActionTypes.LOGIN),
    map((action: Login) => action.payload),
    switchMap(payload => {
      return this.authService.login(payload.usernameOrEmail, payload.password).pipe(
        map((response) => {
          if (!response.user) {
            throw new Error('Login failed.');
          }
          return new LoginSuccess(response);
        }),
        catchError((error) => {
          console.error(error);
          return of(new LoginFailure(error));
        })
      );
    })
  );

  @Effect({ dispatch: false })
  loginSuccess$: Observable<any> = this.actions$.pipe(
    ofType(AuthActionTypes.LOGIN_SUCCESS),
    tap((user) => {
      localStorage.setItem('authToken', user.payload.token);
      if (this.router.url === '/login') {
        this.router.navigateByUrl('/');
      }
    })
  );

  @Effect({ dispatch: false })
  loginFailure$: Observable<any> = this.actions$.pipe(
    ofType(AuthActionTypes.LOGIN_FAILURE)
  );
}
