import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import {
  MatIconModule, MatToolbarModule
} from '@angular/material';
import { Store } from '@ngrx/store';
import { empty } from 'rxjs';
import { State } from './store';

const storeStub: Partial<Store<State>> = {
  pipe: () => empty()
};

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule, MatIconModule, MatToolbarModule
      ],
      declarations: [
        AppComponent
      ],
      providers: [{ provide: Store, useValue: storeStub }]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'wac-app'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('wac-app');
  });
});
