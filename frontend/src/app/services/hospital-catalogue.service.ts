import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Ambulance } from '@store/ambulance/ambulance.model';
import { Disease } from '@store/disease/disease.model';
import { Observable } from 'rxjs';
import { Shift } from '@store/shift/shift.model';
import { User } from '@app/store/user/user.model';

@Injectable({
  providedIn: 'root'
})
export class HospitalCatalogueService {

  constructor(
    private client: HttpClient,
    @Inject('HOSPITAL_CATALOGUE_SERVICE_ENDPOINT') readonly ENDPOINT: string
  ) { }

  /**
   * Get the list of available ambulances
   * @param ids - optional list of ambulance ids
   */
  public getAmbulances(ids?: string[]): Observable<Ambulance[]> {
    return this.client.get<Ambulance[]>(`${this.ENDPOINT}/hospital-catalogue/ambulances`, {
      params: {
        ids: ids || []
      }
    });
  }

  /**
   * Get an ambulance by id
   * @param id - ambulance id
   */
  public getAmbulance(id: string | number): Observable<Ambulance> {
    return this.client.get<Ambulance>(`${this.ENDPOINT}/hospital-catalogue/ambulances/${id}`);
  }

  /**
   * Get list of diseases
   * @param id - ambulance id
   */
  public getDiseases(q?: string): Observable<Disease[]> {
    return this.client.get<Disease[]>(`${this.ENDPOINT}/hospital-catalogue/diseases`, { params: { q } });
  }

  /**
   * Get list of diseases
   * @param id - ambulance id
   */
  public getDisease(id: number): Observable<Disease[]> {
    return this.client.get<Disease[]>(`${this.ENDPOINT}/hospital-catalogue/diseases/${id}`);
  }

  /**
   * Get list of shifts for specified ambulance
   * @param ambulanceId
   */
  public getShiftData(ambulanceId: number, date: Date): Observable<Shift> {
    const params = new HttpParams({
      fromObject: { date: date.toISOString() }
    });

    return this.client.get<Shift>(`${this.ENDPOINT}/hospital-catalogue/shifts/${ambulanceId}`, { params });
  }

  /**
   * Get user data
   * @param ambulanceId
   */
  public getUserData(userId: number): Observable<User> {
    return this.client.get<User>(`${this.ENDPOINT}/hospital-catalogue/user/${userId}`);
  }
}
