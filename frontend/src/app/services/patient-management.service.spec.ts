import { TestBed } from '@angular/core/testing';

import { PatientManagementService } from './patient-management.service';
import { HttpClientModule } from '@angular/common/http';

describe('PatientManagementService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientModule
    ],
    providers: [
      { provide: 'HOSPITAL_CATALOGUE_SERVICE_ENDPOINT', useValue: 'http://dummy.io' },
      { provide: 'APPOINTMENTS_SCHEDULER_SERVICE_ENDPOINT', useValue: 'http://dummy.io' },
      { provide: 'HEALTH_CARD_SERVICE_ENDPOINT', useValue: 'http://dummy.io' }
    ]
  }));

  it('should be created', () => {
    const service: PatientManagementService = TestBed.get(PatientManagementService);
    expect(service).toBeTruthy();
  });
});
