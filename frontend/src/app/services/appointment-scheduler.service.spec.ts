import { TestBed } from '@angular/core/testing';
import { AppointmentSchedulerService } from './appointment-scheduler.service';
import { HttpClient } from '@angular/common/http';
import { mockAppointmentsList } from './fake-data';
import { Appointment } from '@app/store/appointment/appointment.model';
import { asyncData, make404, asyncError } from '@app/testing';

describe('AppointmentSchedulerService', () => {
  let testSubject: AppointmentSchedulerService;
  let httpClientSpy: { get: jasmine.Spy };

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    TestBed.configureTestingModule({
      imports: [],
      providers: [
        { provide: HttpClient, useValue: httpClientSpy },
        { provide: 'HOSPITAL_CATALOGUE_SERVICE_ENDPOINT', useValue: 'http://dummy.io' },
        { provide: 'APPOINTMENTS_SCHEDULER_SERVICE_ENDPOINT', useValue: 'http://dummy.io' },
        { provide: 'HEALTH_CARD_SERVICE_ENDPOINT', useValue: 'http://dummy.io' }
      ]
    });

    testSubject = TestBed.get(AppointmentSchedulerService);
  });

  it('should be created', () => {
    const service: AppointmentSchedulerService = TestBed.get(AppointmentSchedulerService);
    expect(service).toBeTruthy();
  });

  it('should call server once and return expected appointments', async () => {
    // given
    const ambulanceId = mockAppointmentsList[0].ambulanceId;
    const dateFrom = mockAppointmentsList[0].timeStart;
    const dateTo = mockAppointmentsList[mockAppointmentsList.length - 1].timeEnd;

    const expectedAppointments: Appointment[] = mockAppointmentsList;
    httpClientSpy.get.and.returnValue(asyncData(mockAppointmentsList));

    // when
    const result: Appointment[] = await testSubject.getAmbulanceAppointments(ambulanceId, dateFrom, dateTo).toPromise();

    // then
    expect(result).toEqual(expectedAppointments, 'Returned appointments list is different from the expected one');
    expect(httpClientSpy.get.calls.count()).toBe(1, 'Exactly one call is required for getting the appointments list');
  });

  it('should throw a 404 error if the ambulance was not found', async () => {
    // given
    const ambulanceId = -1;
    const dateFrom = mockAppointmentsList[0].timeStart;
    const dateTo = mockAppointmentsList[mockAppointmentsList.length - 1].timeEnd;

    const errorResponse = make404();
    httpClientSpy.get.and.returnValue(asyncError(errorResponse));

    // when
    testSubject.getAmbulanceAppointments(ambulanceId, dateFrom, dateTo).subscribe(
      // then
      ambulance => fail('Expected an error, not a payload'),
      error => expect(error.status).toBe(404)
    );
  });
});
