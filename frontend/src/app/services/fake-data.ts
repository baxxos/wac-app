import { Ambulance } from '../store/ambulance/ambulance.model';
import { User } from '../store/user/user.model';
import { AmbulanceSchedule } from '../store/ambulance-schedule/ambulance-schedule.model';
import { Disease } from '../store/disease/disease.model';
import { Appointment } from '@app/store/appointment/appointment.model';

export const ambulanceMockList: Ambulance[] = [
    {
        id: 1,
        name: 'Osteoportická ambulancia pre dospelých',
        description: '',
        room_identifier: '1.1'
    },
    {
        id: 2,
        name: 'Skoliotická ambulancia',
        description: '',
        room_identifier: '1.2'
    },
    {
        id: 3,
        name: 'Anesteziologická ambulancia',
        description: '',
        room_identifier: '1.3'
    },
    {
        id: 4,
        name: 'Interná ambulancia',
        description: '',
        room_identifier: '2.1'
    },
    {
        id: 5,
        name: 'Geriatrická ambulancia',
        description: '',
        room_identifier: '1.2'
    }
];

export const diseasesMockList: Disease[] = [
    { id: 1, name: 'Influenza', severity: 1 },
    { id: 2, name: 'Cancer', severity: 10 },
    { id: 3, name: 'Hickups', severity: 2 }
]

export const mockUserList: User[] = [
    {
        id: 1,
        firstname: 'John',
        lastname: 'Healer',
        username: 'healer',
        type: 'doctor'
    }, {
        id: 2,
        firstname: 'Ľudovít',
        lastname: 'Štúr',
        username: 'tracer',
        type: 'doctor'
    }, {
        id: 3,
        firstname: 'James',
        lastname: 'Supportman',
        username: 'supportman',
        type: 'doctor'
    }, {
        id: 4,
        firstname: 'Aaron',
        lastname: 'Kneewhacker',
        username: 'kneewhacker',
        type: 'doctor'
    }, {
        id: 5,
        firstname: 'Alan',
        lastname: 'Turing',
        username: 'turing',
        type: 'patient'
    }, {
        id: 6,
        firstname: 'Albert',
        lastname: 'Tesla',
        username: 'musk',
        type: 'patient'
    },
];

export const mockAppointmentsList: Appointment[] = [
    {
        id: 1,
        ambulanceId: 1,
        patientId: 1,
        patientDiseaseId: null,
        notes: 'First appointment',
        reserved: true,
        timeStart: new Date('2020-04-11T10:00:00'),
        timeEnd: new Date('2020-04-11T11:00:00'),
    },
    {
        id: 2,
        ambulanceId: 1,
        patientId: 2,
        patientDiseaseId: null,
        notes: 'Second appointment',
        reserved: true,
        timeStart: new Date('2020-04-11T11:00:00'),
        timeEnd: new Date('2020-04-11T12:00:00'),
    }
]

function generateSchedule(): AmbulanceSchedule[] {
    let schedule = [];
    const doctors = mockUserList.filter(item => item.type === 'doctor')
    const patients = mockUserList.filter(item => item.type === 'patient')
    for (let i = 0; i < ambulanceMockList.length; i++) {
        const ambulance = ambulanceMockList[i];
        const date = new Date();
        for (let day = 0; day <= 3; day++) {
            const doctor = doctors[Math.floor(Math.random() * doctors.length)];
            const patient = patients[Math.floor(Math.random() * patients.length)];
            date.setDate(date.getDate() + 1);
            for (let hour = 8; hour <= 15; hour++) {
                const reservationMade = Math.random() >= 0.5;
                date.setHours(hour, 0, 0, 0);
                const fromDate = new Date(date.getTime());
                const toDate = new Date(date.getTime());
                toDate.setHours(hour + 1, 0, 0, 0);
                const entry = {
                    ambulanceId: ambulance.id,
                    from: fromDate,
                    to: toDate,
                    reserved: reservationMade,
                    doctor_id: doctor.id,
                    doctor,
                    patient_id: reservationMade ? patient.id : null,
                    patient: reservationMade ? patient : null,
                };
                schedule.push(entry);
            }
        }
    }

    return schedule;
}

export const ambulanceScheduleMockList: AmbulanceSchedule[] = generateSchedule();
