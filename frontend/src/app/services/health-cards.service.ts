import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PatientDisease } from '@app/store/patient-disease/patient-disease.model';
import { Store, select } from '@ngrx/store';
import { State, selectDisease, selectAuth } from '@app/store';
import { Observable, of } from 'rxjs';
import { mergeMap, map, share } from 'rxjs/operators';
import * as fromDisease from '@store/disease/disease.reducer';

@Injectable({
  providedIn: 'root'
})
export class HealthCardsService {

  constructor(
    private client: HttpClient,
    @Inject('HEALTH_CARD_SERVICE_ENDPOINT') readonly ENDPOINT: string,
    private readonly store: Store<State>
  ) { }

  /**
   * Create a new patient disease record
   * @param record
   */
  public createPatientDiseaseRecord(record: PatientDisease): Observable<PatientDisease> {
    return this.client
      .post<PatientDisease>(`${this.ENDPOINT}/health-cards`, record)
      .pipe(this.matchRecordDisease());
  }

  /**
   * Get a patient disease record by id
   *
   * @param id
   */
  public getPatientDiseaseRecord(id: number): Observable<PatientDisease> {
    return this.client
      .get<PatientDisease>(`${this.ENDPOINT}/health-cards/${id}`)
      .pipe(share())
      .pipe(this.matchRecordDisease());
  }

  /**
   * Generate a lookup function to match a patient disease record to a disease
   */
  public matchRecordDisease() {
    return mergeMap((record: PatientDisease) => {
      return record.id ?this.store.pipe(
        select(selectDisease),
        select(fromDisease.adapter.getSelectors().selectAll),
        map(diseases => {
          record.disease = diseases.find(d => d.id === record.disease_id);
          return record;
        })
      ) : of(record);
    });
  }

  /**
   * Find a patient - disease record by user and disease ids
   *
   * @param userId
   * @param diseaseId
   */
  public findPatientDiseaseRecord(userId: number, diseaseId: number) {
    return this.client
      .get<PatientDisease>(`${this.ENDPOINT}/health-cards/users/${userId}/diseases/${diseaseId}`)
      .pipe(this.matchRecordDisease());
  }

  /**
   * Update a patient disease record
   *
   * @param record
   */
  public updatePatientDiseaseRecord(record: PatientDisease): Observable<PatientDisease> {
    return this.client
      .put<PatientDisease>(`${this.ENDPOINT}/health-cards/${record.id}`, record)
      .pipe(this.matchRecordDisease());
  }

  /**
   * Update or insert a new patient - disease record
   *
   * @param record
   */
  public upsertPatientDiseaseRecord(record: PatientDisease): Observable<PatientDisease> {
    return record.id ? this.updatePatientDiseaseRecord(record) : this.createPatientDiseaseRecord(record);
  }

  /**
   * Delete a patient disease record
   *
   * @param record
   */
  public deletePatientDiseaseRecord(record: PatientDisease) {
    return this.client.delete(`${this.ENDPOINT}/health-cards/record.id`);
  }

  /**
   * Get all users health records by id
   *
   * @param userId
   */
  public getUsersHealthCard(userId: number): Observable<PatientDisease[]> {
    return this.client.get<PatientDisease[]>(`${this.ENDPOINT}/health-cards/users/${userId}`).pipe(
      mergeMap(records => {
        return this.store.pipe(
          select(selectDisease),
          select(fromDisease.adapter.getSelectors().selectAll),
          map(diseases => records.map(record => {
            record.disease = diseases.find(d => d.id === record.disease_id);
            return record;
          }))
        );
      })
    );
  }
}
