import { TestBed } from '@angular/core/testing';

import { HealthCardsService } from './health-cards.service';
import { HttpClientModule } from '@angular/common/http';
import { EMPTY } from 'rxjs';
import { Store } from '@ngrx/store';
import { State } from '@app/store';

const storeStub: Partial<Store<State>> = {
  pipe: () => EMPTY
};

describe('HealthCardsService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientModule
    ],
    providers: [
      { provide: Store, useValue: storeStub },
      { provide: 'HOSPITAL_CATALOGUE_SERVICE_ENDPOINT', useValue: 'http://dummy.io' },
      { provide: 'APPOINTMENTS_SCHEDULER_SERVICE_ENDPOINT', useValue: 'http://dummy.io' },
      { provide: 'HEALTH_CARD_SERVICE_ENDPOINT', useValue: 'http://dummy.io' }
    ]
  }));

  it('should be created', () => {
    const service: HealthCardsService = TestBed.get(HealthCardsService);
    expect(service).toBeTruthy();
  });
});
