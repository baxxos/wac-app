import { TestBed } from '@angular/core/testing';
import { asyncData, asyncError, make404 } from '../testing';

import { HospitalCatalogueService } from './hospital-catalogue.service';
import { HttpClient } from '@angular/common/http';
import { Ambulance } from '../store/ambulance/ambulance.model';
import { Disease } from '../store/disease/disease.model';
import { ambulanceMockList, diseasesMockList } from './fake-data';



describe('HospitalCatalogueService', () => {
  let testSubject: HospitalCatalogueService;
  let httpClientSpy: { get: jasmine.Spy };

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    TestBed.configureTestingModule({
      providers: [
        { provide: HttpClient, useValue: httpClientSpy },
        { provide: 'HOSPITAL_CATALOGUE_SERVICE_ENDPOINT', useValue: 'http://example.com' }
      ]
    });

    testSubject = TestBed.get(HospitalCatalogueService);
  });

  it('should be created', () => {
    expect(testSubject).toBeTruthy();
  });

  it('should return expected ambulances (HttpClient called once)', async () => {
    // given
    const expectedAmbulances: Ambulance[] = ambulanceMockList;

    httpClientSpy.get.and.returnValue(asyncData(ambulanceMockList));

    // when
    const result: Ambulance[] = await testSubject.getAmbulances().toPromise();

    // then
    expect(result).toEqual(expectedAmbulances, 'expected ambulances')
    expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
  });

  it('should return a 404 error when the server cant find the ambulance', () => {
    // given
    const id = 1;
    const errorResponse = make404();
    httpClientSpy.get.and.returnValue(asyncError(errorResponse));

    // when
    testSubject.getAmbulance(id).subscribe(
      // then
      ambulance => fail('expected an error, not an ambulance'),
      error => expect(error.status).toBe(404)
    );
  });

  it('should return expected diseases (HttpClient called once)', async () => {
    const expectedDiseases: Disease[] = diseasesMockList;

    httpClientSpy.get.and.returnValue(asyncData(expectedDiseases));

    // when
    const result: Disease[] = await testSubject.getDiseases().toPromise();

    // then
    expect(result).toEqual(expectedDiseases, 'expected diseases');
    expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
  });

  it('should return a 404 error when the server cant find the disease', () => {
    // given
    const id = 1;
    const errorResponse = make404();
    httpClientSpy.get.and.returnValue(asyncError(errorResponse));

    // when
    testSubject.getDisease(id).subscribe(
      // then
      disease => fail('expected an error, not a response'),
      error => expect(error.status).toBe(404)
    );
  });
});
