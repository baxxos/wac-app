import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators'
import { mockUserList } from './fake-data';
import { User } from '../store/user/user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(
    private client: HttpClient,
    @Inject('APPOINTMENTS_SCHEDULER_SERVICE_ENDPOINT') readonly ENDPOINT: string
  ) { }

  public getToken(): string {
    return localStorage.getItem('authToken');
  }

  login(emailOrUsername: string, password: string): Observable<any> {
    let user: User;

    user = mockUserList.find(item => item.email === emailOrUsername || item.username === emailOrUsername)
    if (user) {
      sessionStorage.setItem('username', user.username);
      const userId = user.id;
      sessionStorage.setItem('userId', userId.toString());
    }
    // Replace with a HTTP response later
    return of({
      user: user,
      token: 'authToken'
    }).pipe(delay(500));
  }

}
