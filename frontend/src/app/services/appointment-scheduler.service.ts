import { Injectable, Inject } from '@angular/core';
import { Ambulance } from '../store/ambulance/ambulance.model';
import { AmbulanceSchedule } from '../store/ambulance-schedule/ambulance-schedule.model';

import { Observable, of } from 'rxjs';
import { delay, map } from 'rxjs/operators';

import { ambulanceMockList, ambulanceScheduleMockList } from './fake-data';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Appointment } from '@app/store/appointment/appointment.model';


@Injectable({
  providedIn: 'root'
})
export class AppointmentSchedulerService {

  private ambulanceMockList: Ambulance[] = ambulanceMockList;
  private ambulanceScheduleMockList: AmbulanceSchedule[] = ambulanceScheduleMockList;
  private appointmentsList$: Appointment[];

  constructor(
    private client: HttpClient,
    @Inject('APPOINTMENTS_SCHEDULER_SERVICE_ENDPOINT') readonly ENDPOINT: string
  ) { }

  /**
   * Get all appointments
   */
  public getAppointments(): Observable<Appointment[]> {
    return this.client.get<Appointment[]>(`${this.ENDPOINT}/appointments`)
      .pipe(this.dateCastingPipeline());
  }

  /**
   * Get ambulance appointments within a certain range
   *
   * @param ambulanceId
   * @param dateFrom
   * @param dateTo
   * @param userId?
   */
  public getAmbulanceAppointments(ambulanceId: number, dateFrom: Date, dateTo: Date, userId?: number): Observable<Appointment[]> {
    const payload = {
      dateFrom: dateFrom.toISOString(),
      dateTo: dateTo.toISOString(),
    }

    if (userId) { payload['userId'] = userId; }
    const params = new HttpParams({ fromObject: payload });

    return this.client
      .get<Appointment[]>(`${this.ENDPOINT}/appointments/ambulance/${ambulanceId}`, { params })
      .pipe(this.dateCastingPipeline());
  }

  /**
   * Get appointments by patient disease record
   */
  public getAppointmentsByPatientDiseaseRecord(patientDiseaseId: number) {
    return this.client
      .get<Appointment[]>(`${this.ENDPOINT}/appointments/patient-disease/${patientDiseaseId}`)
      .pipe(this.dateCastingPipeline());
  }

  /**
   * Update an appointment
   *
   * @param appointment
   */
  public updateAppointment(appointment: Appointment): Observable<Appointment> {
    return this.client.put<Appointment>(`${this.ENDPOINT}/appointments/${appointment.id}`, appointment);
  }

  /**
   * Create an appointment
   */
  public createAppointment(ambulanceId: number, appointment: Appointment) {
    const payload = {
      id: 0,
      ambulanceId,
      patientId: appointment.patientId,
      patientDiseaseId: appointment.patientDiseaseId,
      timeStart: appointment.timeStart.toISOString(),
      timeEnd: appointment.timeEnd.toISOString(),
      notes: appointment.notes
    };
    return this.client.post(`${this.ENDPOINT}/appointments/ambulance/` + ambulanceId, payload, { observe: 'response' });
  }

  /**
   * Delete appointment at an ambulance at the specified date
   *
   * @param ambulanceId
   * @param dateFrom
   * @param dateTo
   */
  public deleteAppointmentByDate(ambulanceId: number, dateFrom: Date, dateTo: Date) {
    return this.client.delete(`${this.ENDPOINT}/appointments/ambulance/` + ambulanceId, {
      params: {
        dateFrom: dateFrom.toISOString(),
        dateTo: dateTo.toISOString()
      }
    });
  }

  /**
   * Delete an appointment with specified ID
   *
   * @param appointmentId
   */
  public deleteAppointmentById(appointmentId: number) {
    return this.client.delete(`${this.ENDPOINT}/appointments/${appointmentId}`, { observe: 'response' });
  }

  /**
   * Return an observable pipe that casts appointment dates to Date objects
   */
  public dateCastingPipeline() {
    return map((appointments: Appointment[]) => appointments.map(a => {
      a.timeStart = new Date(a.timeStart);
      a.timeEnd = new Date(a.timeEnd);
      return a;
    }));
  }
}
