import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AmbulanceSchedule } from '../store/ambulance-schedule/ambulance-schedule.model';
import { ambulanceScheduleMockList } from './fake-data';

import { Observable, of } from 'rxjs';
import { delay, map, filter } from 'rxjs/operators';
import { Shift } from '@app/store/shift/shift.model';

@Injectable({
  providedIn: 'root'
})
export class PatientManagementService {

  private ambulanceScheduleMockList: AmbulanceSchedule[] = ambulanceScheduleMockList;

  constructor(
    private client: HttpClient,
    @Inject('HOSPITAL_CATALOGUE_SERVICE_ENDPOINT') readonly ENDPOINT: string
  ) { }

   /**
   * Get the ambulance appointment schedule for specific doctor
   *
   * @param ambulanceId
   */
  public getAmbulanceSchedule(doctorId: number): Observable<AmbulanceSchedule[]> {
    return of(this.ambulanceScheduleMockList.filter(entry => {
      return entry.doctor.id === doctorId;
    })).pipe(delay(500));
  }

   /**
   * Get shifts for specific doctor
   *
   * @param doctor_id
   */
  public getShifts(doctor_id: number): Observable<Shift[]> {

    return this.client.get<Shift[]>(`${this.ENDPOINT}/hospital-catalogue/shifts`).pipe(map( shifts =>
      shifts.filter(data => data.id_doctor === doctor_id)))
  }

}
