import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localeSk from '@angular/common/locales/sk';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PatientManagementComponent } from './components/patient-management/patient-management.component';
import { TreatmentsTrackerComponent } from './components/treatments-tracker/treatments-tracker.component';
import { AppointmentSchedulerComponent } from './components/appointment-scheduler/appointment-scheduler.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatInputModule, MatIconModule, MatToolbarModule, MatButtonModule, MatFormFieldModule,
  MatTableModule, MatGridListModule, MatCardModule, MatDatepickerModule, MatNativeDateModule, MatDialogModule,
  MatProgressSpinnerModule, MatSelectModule, MatTooltipModule, MatListModule, MatDividerModule
} from '@angular/material';
import { StoreModule } from '@ngrx/store';
import { reducers, metaReducers } from './store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { EffectsModule } from '@ngrx/effects';
import { AppEffects } from './effects/app.effects';

import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { AuthService } from './services/auth.service';
import { AuthEffects } from './effects/auth.effects';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './components/login/login.component';
import { FormsModule } from '@angular/forms';
import { NewAppointmentModalComponent } from './components/appointment-scheduler/new-appointment-modal/new-appointment-modal.component';
import { CancelAppointmentModalComponent } from './components/cancel-appointment-modal/cancel-appointment-modal.component';
import { AppointmentsTableComponent } from './components/appointments-table/appointments-table.component';

registerLocaleData(localeSk);

@NgModule({
  declarations: [
    AppComponent,
    PatientManagementComponent,
    TreatmentsTrackerComponent,
    AppointmentSchedulerComponent,
    LoginComponent,
    NewAppointmentModalComponent,
    CancelAppointmentModalComponent,
    AppointmentsTableComponent
  ],
  entryComponents: [
    NewAppointmentModalComponent,
    CancelAppointmentModalComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    MatIconModule,
    MatToolbarModule,
    MatButtonModule,
    MatFormFieldModule,
    MatTableModule,
    MatGridListModule,
    MatCardModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
    MatListModule,
    MatDividerModule,
    StoreModule.forRoot(reducers, { metaReducers }),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    EffectsModule.forRoot([AppEffects]),
    StoreRouterConnectingModule.forRoot(),
    EffectsModule.forFeature([AuthEffects])
  ],
  providers: [
    AuthService,
    { provide: LOCALE_ID, useValue: 'sk-SK' },
    { provide: 'HOSPITAL_CATALOGUE_SERVICE_ENDPOINT', useValue: environment.services.hospitalCatalogue },
    { provide: 'APPOINTMENTS_SCHEDULER_SERVICE_ENDPOINT', useValue: environment.services.appointmentsScheduler },
    { provide: 'AUTH_SERVICE_ENDPOINT', useValue: 'http://not_implemented' },
    { provide: 'HEALTH_CARD_SERVICE_ENDPOINT', useValue: environment.services.healthCards }

  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
