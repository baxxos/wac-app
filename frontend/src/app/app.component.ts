import { Component } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { State } from './store';
import { User } from './store/user/user.model';
import { Observable } from 'rxjs';
import { Login } from './store/auth/auth.actions';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  public title = 'wac-app';
  public user$: Observable<User>;

  constructor(
    private readonly store: Store<State>,
    public router:Router
  ) {
    if(sessionStorage.getItem('username') != null) {
      const usernameOrEmail = sessionStorage.getItem('username')
      const password = " "
      const payload = {
        usernameOrEmail,
        password
      };
      this.store.dispatch(new Login(payload));
    }

    this.user$ = store.pipe(
      select((state: State) => state.auth.user)
    );
  }

  public onLogout() {
    sessionStorage.removeItem('username')
    document.location.href="/";
  };

  public copyToClipboard(item) {
    document.addEventListener('copy', (e: ClipboardEvent) => {
      e.clipboardData.setData('text/plain', (item));
      e.preventDefault();
      document.removeEventListener('copy', null);
    });
    document.execCommand('copy');
  }
}
