import { Routes } from '@angular/router';

import { AppointmentSchedulerComponent } from './components/appointment-scheduler/appointment-scheduler.component';
import { PatientManagementComponent } from './components/patient-management/patient-management.component';
import { TreatmentsTrackerComponent } from './components/treatments-tracker/treatments-tracker.component';
import { LoginComponent } from './components/login/login.component';

// TODO: Redirect un-authenticated users to to the login screen somehow
export const routes: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'appointment-scheduler', component: AppointmentSchedulerComponent },
    { path: 'patient-management', component: PatientManagementComponent },
    { path: 'treatments-tracker', component: TreatmentsTrackerComponent }
]