pushd appointments-scheduler/
    dotnet restore
    dotnet watch run &
popd
pushd health-cards/
    dotnet restore
    dotnet watch run &
popd
pushd hospital-catalogue/
    dotnet restore
    
    # When the script is killed e.g. with CTRL-C, make sure all spawned subprocesses die too
    trap "trap - SIGTERM && kill -- -$$" SIGINT SIGTERM EXIT

    # make sure to start the last subprocess in the foreground
    dotnet watch run 
popd