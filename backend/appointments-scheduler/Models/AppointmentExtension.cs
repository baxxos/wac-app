using System;

namespace WAC.Legends.AppointmentsScheduler.Models
{
    static class AppointmentExtension
    {
        static public bool ValidateTimeAgainstOpeningHours(
            this Appointment appointment, DateTime openingHoursStart, DateTime openingHoursEnd)
        {
            return 
                appointment != null &&
                appointment.TimeStart.ToLocalTime().TimeOfDay >= openingHoursStart.ToLocalTime().TimeOfDay &&
                appointment.TimeStart.ToLocalTime().TimeOfDay <= openingHoursEnd.ToLocalTime().TimeOfDay &&
                appointment.TimeEnd.ToLocalTime().TimeOfDay >= openingHoursStart.ToLocalTime().TimeOfDay &&
                appointment.TimeEnd.ToLocalTime().TimeOfDay <= openingHoursEnd.ToLocalTime().TimeOfDay;
        }
    }
}