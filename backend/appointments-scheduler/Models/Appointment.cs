/*
 * Appointment Scheduling Service
 *
 * (Core) Service for scheduling ambulance appointments
 *
 * OpenAPI spec version: 1.0.0
 * Contact: meluch.m@gmail.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace WAC.Legends.AppointmentsScheduler.Models
{ 
    /// <summary>
    /// Describes a doctor&#39;s appointment in a specified ambulance
    /// </summary>
    [DataContract]
    public partial class Appointment : IEquatable<Appointment>
    { 
        /// <summary>
        /// Gets or Sets Id
        /// </summary>
        [DataMember(Name="id")]
        public int Id { get; set; }

        /// <summary>
        /// Gets or Sets AmbulanceId
        /// </summary>
        [Required]
        [DataMember(Name="ambulanceId")]
        public int AmbulanceId { get; set; }

        /// <summary>
        /// Gets or Sets PatientId
        /// </summary>
        [Required]
        [DataMember(Name="patientId")]
        public int PatientId { get; set; }

        /// <summary>
        /// Gets or Sets PatientDiseaseId
        /// </summary>
        [Required]
        [DataMember(Name="patientDiseaseId")]
        public int? PatientDiseaseId { get; set; }

        /// <summary>
        /// Gets or Sets TimeStart
        /// </summary>
        [Required]
        [DataMember(Name="timeStart")]
        public DateTime TimeStart { get; set; }

        /// <summary>
        /// Gets or Sets TimeEnd
        /// </summary>
        [Required]
        [DataMember(Name="timeEnd")]
        public DateTime TimeEnd { get; set; }

        /// <summary>
        /// Gets or Sets Notes
        /// </summary>
        [DataMember(Name="notes")]
        public string Notes { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Appointment {\n");
            sb.Append("  Id: ").Append(Id).Append("\n");
            sb.Append("  AmbulanceId: ").Append(AmbulanceId).Append("\n");
            sb.Append("  PatientId: ").Append(PatientId).Append("\n");
            sb.Append("  patientDiseaseId: ").Append(PatientDiseaseId).Append("\n");
            sb.Append("  TimeStart: ").Append(TimeStart).Append("\n");
            sb.Append("  TimeEnd: ").Append(TimeEnd).Append("\n");
            sb.Append("  Notes: ").Append(Notes).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((Appointment)obj);
        }

        /// <summary>
        /// Returns true if Appointment instances are equal
        /// </summary>
        /// <param name="other">Instance of Appointment to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Appointment other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return 
                (
                    Id == other.Id ||
                    Id != null &&
                    Id.Equals(other.Id)
                ) && 
                (
                    AmbulanceId == other.AmbulanceId ||
                    AmbulanceId != null &&
                    AmbulanceId.Equals(other.AmbulanceId)
                ) && 
                (
                    PatientId == other.PatientId ||
                    PatientId != null &&
                    PatientId.Equals(other.PatientId)
                ) && 
                (
                    TimeStart == other.TimeStart ||
                    TimeStart != null &&
                    TimeStart.Equals(other.TimeStart)
                ) && 
                (
                    TimeEnd == other.TimeEnd ||
                    TimeEnd != null &&
                    TimeEnd.Equals(other.TimeEnd)
                ) && 
                (
                    Notes == other.Notes ||
                    Notes != null &&
                    Notes.Equals(other.Notes)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                // Suitable nullity checks etc, of course :)
                    if (Id != null)
                    hashCode = hashCode * 59 + Id.GetHashCode();
                    if (AmbulanceId != null)
                    hashCode = hashCode * 59 + AmbulanceId.GetHashCode();
                    if (PatientId != null)
                    hashCode = hashCode * 59 + PatientId.GetHashCode();
                    if (TimeStart != null)
                    hashCode = hashCode * 59 + TimeStart.GetHashCode();
                    if (TimeEnd != null)
                    hashCode = hashCode * 59 + TimeEnd.GetHashCode();
                    if (Notes != null)
                    hashCode = hashCode * 59 + Notes.GetHashCode();
                return hashCode;
            }
        }

        #region Operators
        #pragma warning disable 1591

        public static bool operator ==(Appointment left, Appointment right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Appointment left, Appointment right)
        {
            return !Equals(left, right);
        }

        #pragma warning restore 1591
        #endregion Operators
    }
}
