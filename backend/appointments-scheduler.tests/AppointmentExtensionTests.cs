using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using WAC.Legends.AppointmentsScheduler.Models;
using WAC.Legends.AppointmentsScheduler.Controllers;

namespace appointments_scheduler.tests
{
    [TestClass]
    public class AppointmentExtensionTests
    {
        [DataTestMethod]
        [DynamicData("SingleAppointmentScenario")]
        public void Test_Verify_Appointment_Starting_Before_Opening_Hours(Appointment appointment)
        {
            appointment.TimeStart = AppointmentsApiController.globalOpeningHours["timeStart"].AddHours(-1);
            bool validationResult = ValidateAgainstGlobalOpeningHours(appointment);
            Assert.IsFalse(validationResult);
        }

        [DataTestMethod]
        [DynamicData("SingleAppointmentScenario")]
        public void Test_Verify_Appointment_Ending_Before_Opening_Hours(Appointment appointment)
        {
            appointment.TimeEnd = AppointmentsApiController.globalOpeningHours["timeStart"].AddHours(-1);
            bool validationResult = ValidateAgainstGlobalOpeningHours(appointment);
            Assert.IsFalse(validationResult);
        }

        [DataTestMethod]
        [DynamicData("SingleAppointmentScenario")]
        public void Test_Verify_Appointment_Starting_After_Opening_Hours(Appointment appointment)
        {
            appointment.TimeStart = AppointmentsApiController.globalOpeningHours["timeEnd"].AddHours(1);
            bool validationResult = ValidateAgainstGlobalOpeningHours(appointment);
            Assert.IsFalse(validationResult);
        }

        [DataTestMethod]
        [DynamicData("SingleAppointmentScenario")]
        public void Test_Verify_Appointment_Ending_After_Opening_Hours(Appointment appointment)
        {
            appointment.TimeEnd = AppointmentsApiController.globalOpeningHours["timeEnd"].AddHours(1);
            bool validationResult = ValidateAgainstGlobalOpeningHours(appointment);
            Assert.IsFalse(validationResult);
        }

        [DataTestMethod]
        [DynamicData("SingleAppointmentScenario")]
        public void Test_Verify_Appointment_During_Opening_Hourse(Appointment appointment)
        {
            bool validationResult = ValidateAgainstGlobalOpeningHours(appointment);
            Assert.IsTrue(validationResult);
        }

        private bool ValidateAgainstGlobalOpeningHours(Appointment appointment)
        {
            return appointment.ValidateTimeAgainstOpeningHours(
                AppointmentsApiController.globalOpeningHours["timeStart"],
                AppointmentsApiController.globalOpeningHours["timeEnd"]);
        }

        static public IEnumerable<object[]> SingleAppointmentScenario
        {
            get
            {
                var appointments = new Appointment[] {
                    new Appointment {
                        Id = 0,
                        AmbulanceId = 0,
                        PatientId = 0,
                        PatientDiseaseId = 0,
                        TimeStart = AppointmentsApiController.globalOpeningHours["timeStart"],
                        TimeEnd = AppointmentsApiController.globalOpeningHours["timeStart"].AddHours(1),
                        Notes = null
                    }
                };

                return appointments.Select(_ => new object[] { _ });
            }
        }
    }
}
