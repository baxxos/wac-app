using System.IO;
using WAC.Legends.HospitalCatalogue.Models;
using WAC.Legends.HospitalCatalogue.Services;
using LiteDB;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System;

namespace WAC.Legends.HospitalCatalogue.Services
{
    /// <summary>
    /// Abstraction of the data repository keeping data model persistent
    ///
    /// Responsibilities:
    /// * CRUD operations over data maodel
    /// * Searching and filtering durring data retrieval
    /// </summary>
    public interface IDataRepository
    {

        IEnumerable<Ambulance> GetAmbulances();

        Ambulance GetAmbulance(int ambulanceId);

        IEnumerable<Disease> GetDiseases();

        Disease GetDisease(int diseaseId);


        IEnumerable<User> GetUsers();

        User GetUser(int userId);

        Shift GetAmbulanceShift(int ambulanceId, DateTime date);

        IEnumerable<Shift> GetShifts();

        void SeedDB();

    }
    class DataRepository : IDataRepository
    {
        private readonly LiteDatabase liteDb;
        private static readonly string AMBULANCES_COLLECTION = "ambulances";
        private static readonly string DISEASES_COLLECTION = "diseases";
        private static readonly string SHIFTS_COLLECTION = "shifts";
        private static readonly string USERS_COLLECTION = "users";
        public DataRepository(
        IHostingEnvironment environment, IConfiguration configuration)
        {
            string dbPath = configuration["DATA_REPOSITORY_DB"];
            if (dbPath == null || dbPath.Length == 0)
            {
                dbPath = Path.Combine(
                environment.ContentRootPath, "data-repository.litedb");
            }
            this.liteDb = new LiteDatabase(dbPath);
        }

        public void SeedDB()
        {

            if (!this.liteDb.CollectionExists(AMBULANCES_COLLECTION))
            {
                var collection_ambulances = this.liteDb.GetCollection<Ambulance>(AMBULANCES_COLLECTION);
                string[] ambulance_names = { "Osteoportická ambulancia pre dospelých", "Skoliotická ambulancia", "Anesteziologická ambulancia", "Interná ambulancia", "Geriatrická ambulancia" };
                Ambulance[] ambulances = new Ambulance[5];
                for (int i = 0; i < 5; i++)
                {
                    ambulances[i] = new Ambulance
                    {
                        Id = 0,
                        Name = ambulance_names[i],
                        RoomIdentifier = (i * 2).ToString(),
                        Description = "Welcome"
                    };
                    collection_ambulances.Insert(ambulances[i]);
                }
            }

            if (!this.liteDb.CollectionExists(DISEASES_COLLECTION))
            {
                var collection_diseases = this.liteDb.GetCollection<Disease>(DISEASES_COLLECTION);

                string[] diseases_names = { "Chrípka", "Hlavová bolesť", "Bolesť ľavého kolena", "Tetanus", "Arachnofóbia", "Hypertenzia" };
                Disease[] diseases = new Disease[6];
                Random rnd = new Random();

                for (int i = 0; i < 6; i++)
                {
                    diseases[i] = new Disease
                    {
                        Id = 0,
                        Name = diseases_names[i],
                        Severity = rnd.Next(1, 11)
                    };
                    collection_diseases.Insert(diseases[i]);
                }
            }

            if (!this.liteDb.CollectionExists(SHIFTS_COLLECTION))
            {
                var collection_shifts = this.liteDb.GetCollection<Shift>(SHIFTS_COLLECTION);

                Shift[] shifts = new Shift[50];
                Random rnd = new Random();

                for (int IdAmbulance = 1; IdAmbulance < 6; IdAmbulance++)
                {
                    for (int i = 0; i < 30; i++)
                    {
                        shifts[i] = new Shift
                        {
                            Id = 0,
                            IdAmbulance = IdAmbulance,
                            IdDoctor = rnd.Next(1, 5),
                            Date = DateTime.Today.AddDays(i)
                        };

                        collection_shifts.Insert(shifts[i]);
                    }
                }
            }

            if (!this.liteDb.CollectionExists(USERS_COLLECTION))
            {
                var collection_users = this.liteDb.GetCollection<User>(USERS_COLLECTION);
                string[] users_first_names = { "John", "Ľudovít", "James", "Aaron", "Alan", "Albert" };
                string[] users_last_names = { "Healer", "Štúr", "Supportman", "Kneewhacker", "Turing", "Tesla" };
                string[] users_usernames = { "healer", "tracer", "supportman", "kneewhacker", "turing", "musk" };
                string[] users_types = { "doctor", "doctor", "doctor", "doctor", "patient", "patient" };
                User[] users = new User[6];
                for (int i = 0; i < 6; i++)
                {
                    users[i] = new User
                    {
                        Id = i + 1,
                        FirstName = users_first_names[i],
                        LastName = users_last_names[i],
                        UserName = users_usernames[i],
                        Type = users_types[i]
                    };
                    collection_users.Insert(users[i]);
                }
            }

        }

        public Ambulance GetAmbulance(int ambulanceId)
        {
            var collection = this.liteDb.GetCollection<Ambulance>(AMBULANCES_COLLECTION);
            return collection.FindById(ambulanceId);
        }

        public IEnumerable<Ambulance> GetAmbulances()
        {
            var collection = this.liteDb.GetCollection<Ambulance>(AMBULANCES_COLLECTION);
            return collection.FindAll();
        }

        public Disease GetDisease(int diseaseId)
        {
            var collection = this.liteDb.GetCollection<Disease>(DISEASES_COLLECTION);
            return collection.FindById(diseaseId);
        }

        public IEnumerable<Disease> GetDiseases()
        {
            var collection = this.liteDb.GetCollection<Disease>(DISEASES_COLLECTION);
            return collection.FindAll();
        }

        public User GetUser(int userId)
        {
            var collection = this.liteDb.GetCollection<User>(USERS_COLLECTION);
            return collection.FindById(userId);
        }

        public IEnumerable<User> GetUsers()
        {
            var collection = this.liteDb.GetCollection<User>(USERS_COLLECTION);
            return collection.FindAll();
        }

        public Shift GetAmbulanceShift(int ambulanceId, DateTime date)
        {
            var collection = this.liteDb.GetCollection<Shift>(SHIFTS_COLLECTION);

            //TODO: filter by ambulanceId and date
            return collection.FindOne(
                x => x.IdAmbulance == ambulanceId &&
                x.Date.ToLocalTime().Date == date.ToLocalTime().Date);
        }

        public IEnumerable<Shift> GetShifts()
        {
            var collection = this.liteDb.GetCollection<Shift>(SHIFTS_COLLECTION);

            //TODO: filter by ambulanceId and date
            return collection.FindAll();
        }

    }
}
namespace Microsoft.Extensions.DependencyInjection
{
    static class DataRepositoryDI
    {
        /// supportive extension function for dependency
        /// injection of the service
        public static IServiceCollection AddDataRepository(
        this IServiceCollection services)
        => services.AddSingleton<IDataRepository, DataRepository>();
    }
}
