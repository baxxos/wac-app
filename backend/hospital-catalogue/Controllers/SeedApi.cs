/*
 * Hospital catalogue service
 *
 * (Supporting) Hospital catalogue service for the managment of generic enetities
 *
 * OpenAPI spec version: 1.0.0
 * Contact: ppisecky@gmail.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;
using Swashbuckle.AspNetCore.SwaggerGen;
using Swashbuckle.AspNetCore.Annotations;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using WAC.Legends.HospitalCatalogue.Attributes;
using WAC.Legends.HospitalCatalogue.Models;
using WAC.Legends.HospitalCatalogue.Services;

namespace WAC.Legends.HospitalCatalogue.Controllers
{
    /// <summary>
    ///
    /// </summary>
    public class SeedApiController : Controller
    {

        private readonly IDataRepository repository;

        public SeedApiController(IDataRepository repository) => this.repository = repository;
        /// <summary>
        /// Seed catalogue
        /// </summary>
        /// <remarks>Fetch a list of all ambulances in the hospital or search for specific ambulances identified by the provided list of ids.</remarks>
        /// <response code="200">List of found ambulances.</response>
        [HttpGet]
        [Route("/hospital-catalogue/seed")]
        [ValidateModelState]
        [SwaggerOperation("SeedDB")]
        public virtual IActionResult SeedDB()
        {
            this.repository.SeedDB();
            return new OkResult();
        }
    }
}
