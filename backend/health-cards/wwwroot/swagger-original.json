{
  "swagger" : "2.0",
  "info" : {
    "description" : "Health card service API definition",
    "version" : "1.0.0",
    "title" : "Health Card Service API",
    "contact" : {
      "email" : "ppisecky@gmail.com"
    },
    "license" : {
      "name" : "Apache 2.0",
      "url" : "http://www.apache.org/licenses/LICENSE-2.0.html"
    }
  },
  "host" : "virtserver.swaggerhub.com",
  "basePath" : "/wac-legends/health-card-service/1.0.0",
  "tags" : [ {
    "name" : "health-cards",
    "description" : "Health card related API"
  } ],
  "schemes" : [ "https", "http" ],
  "paths" : {
    "/healthcards" : {
      "post" : {
        "tags" : [ "health-cards" ],
        "summary" : "Register a new disease case",
        "description" : "Create a record of a new patients disease\n",
        "operationId" : "createPatientDiseaseRecord",
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "in" : "body",
          "name" : "body",
          "description" : "PatientDisease record template",
          "required" : true,
          "schema" : {
            "$ref" : "#/definitions/PatientDisease"
          }
        } ],
        "responses" : {
          "200" : {
            "description" : "The newly created record with its ID filled in",
            "schema" : {
              "$ref" : "#/definitions/PatientDisease"
            }
          },
          "400" : {
            "description" : "Missing mandatory properties"
          },
          "409" : {
            "description" : "The patient to disease binding already exists"
          }
        }
      }
    },
    "/healthcards/{id}" : {
      "get" : {
        "tags" : [ "health-cards" ],
        "summary" : "Get a patient - disease record",
        "description" : "Get a patient-disease record specified in the request path\n",
        "operationId" : "getPatientDiseaseRecord",
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "id",
          "in" : "path",
          "description" : "ID of the patient-disease record",
          "required" : true,
          "type" : "integer",
          "format" : "int32"
        } ],
        "responses" : {
          "200" : {
            "description" : "Record deleted",
            "schema" : {
              "$ref" : "#/definitions/PatientDisease"
            }
          },
          "404" : {
            "description" : "Record not found"
          }
        }
      },
      "put" : {
        "tags" : [ "health-cards" ],
        "summary" : "Get a users' health card",
        "description" : "Update the patient disease record\n",
        "operationId" : "updatePatientDiseaseRecord",
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "id",
          "in" : "path",
          "description" : "ID of the patient-disease record",
          "required" : true,
          "type" : "integer",
          "format" : "int32"
        }, {
          "in" : "body",
          "name" : "body",
          "description" : "PatientDisease record template",
          "required" : true,
          "schema" : {
            "$ref" : "#/definitions/PatientDisease"
          }
        } ],
        "responses" : {
          "200" : {
            "description" : "Record updated",
            "schema" : {
              "$ref" : "#/definitions/PatientDisease"
            }
          },
          "400" : {
            "description" : "Malformed request"
          },
          "404" : {
            "description" : "Record not found"
          }
        }
      },
      "delete" : {
        "tags" : [ "health-cards" ],
        "summary" : "Delete a patient - disease record",
        "description" : "Deleate a patient-disease record specified in the request path\n",
        "operationId" : "deletePatientDiseaseRecord",
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "id",
          "in" : "path",
          "description" : "ID of the patient-disease record",
          "required" : true,
          "type" : "integer",
          "format" : "int32"
        } ],
        "responses" : {
          "200" : {
            "description" : "Record deleted"
          },
          "404" : {
            "description" : "Record not found"
          }
        }
      }
    },
    "/healthcards/users/{user_id}" : {
      "get" : {
        "tags" : [ "health-cards" ],
        "summary" : "Get a users' health card",
        "description" : "Fetch the complete health card for the user/patient specified in the request path\n",
        "operationId" : "getUsersHealthCard",
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "user_id",
          "in" : "path",
          "description" : "ID of the user",
          "required" : true,
          "type" : "integer",
          "format" : "int32"
        } ],
        "responses" : {
          "200" : {
            "description" : "List of patient-disease bindings",
            "schema" : {
              "type" : "array",
              "items" : {
                "$ref" : "#/definitions/PatientDisease"
              }
            }
          },
          "404" : {
            "description" : "User records not found"
          }
        }
      }
    }
  },
  "definitions" : {
    "PatientDisease" : {
      "type" : "object",
      "required" : [ "disease_id", "id", "patient_id" ],
      "properties" : {
        "id" : {
          "type" : "integer",
          "format" : "int32",
          "example" : 1
        },
        "patient_id" : {
          "type" : "integer",
          "format" : "int32",
          "example" : 1
        },
        "disease_id" : {
          "type" : "integer",
          "format" : "int32",
          "example" : 1
        },
        "description" : {
          "type" : "string",
          "example" : "Patient caught the deadly disease while fishing in Burma",
          "description" : "Doctors notes about the patients conditions"
        }
      },
      "example" : {
        "patient_id" : 1,
        "disease_id" : 1,
        "description" : "Patient caught the deadly disease while fishing in Burma",
        "id" : 1
      }
    }
  }
}
