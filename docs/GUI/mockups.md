# User Interface Mockups

## Appointments scheduler

![Appointments scheduler](WAC-UC1.png)

## Treatments tracker

![Treatments tracker](WAC-UC2.png)

## Patient managment

![Patient managment](WAC-UC3.png)