# Microservices Build: Tasks

```yaml
resources:
- repo: self

pool:
  name: Hosted VS2017
steps:
- task: DotNetCoreInstaller@0
  displayName: 'Use .NET Core sdk 2.2.101'
  inputs:
    version: 2.2.101

- task: DotNetCoreCLI@2
  displayName: 'dotnet restore'
  inputs:
    command: restore
    projects: 'backend/*/*.csproj'

- task: DotNetCoreCLI@2
  displayName: 'dotnet build sources'
  inputs:
    projects: |
     backend/hospital-catalogue/hospital-catalogue.csproj
     backend/appointments-scheduler/appointments-scheduler.csproj
     backend/health-cards/health-cards.csproj
    arguments: '--configuration Release --no-restore'

- task: DotNetCoreCLI@2
  displayName: 'dotnet build appointments scheduler tests'
  inputs:
    projects: 'backend/appointments-scheduler.tests/appointments-scheduler.tests.csproj'
    arguments: '--configuration Release --no-restore'

- task: DotNetCoreCLI@2
  displayName: 'dotnet test appointments scheduler service'
  inputs:
    command: test
    projects: 'backend/appointments-scheduler.tests/appointments-scheduler.tests.csproj'
    arguments: '--configuration Release --no-build'

- task: DotNetCoreCLI@2
  displayName: 'dotnet publish'
  inputs:
    command: publish
    publishWebProjects: false
    projects: 'backend/appointments-scheduler.tests/appointments-scheduler.tests.csproj'
    arguments: '--configuration Release --no-build'
    zipAfterPublish: false

- task: DotNetCoreCLI@2
  displayName: 'dotnet publish'
  inputs:
    command: publish
    publishWebProjects: false
    projects: |
     backend\hospital-catalogue\hospital-catalogue.csproj
     backend\appointments-scheduler\appointments-scheduler.csproj
     backend\health-cards\health-cards.csproj
    arguments: '--configuration Release --no-build'
    zipAfterPublish: false

- task: PublishBuildArtifacts@1
  displayName: 'Publish Artifact: HOSPITAL-CATALOGUE-SERVICE-DROP'
  inputs:
    PathtoPublish: 'backend\hospital-catalogue\bin\Release\netcoreapp2.2\publish\'
    ArtifactName: 'HOSPITAL-CATALOGUE-SERVICE-DROP'

- task: PublishBuildArtifacts@1
  displayName: 'Publish Artifact: APPOINTMENTS-SCHEDULER-SERVICE-DROP'
  inputs:
    PathtoPublish: 'backend\appointments-scheduler\bin\Release\netcoreapp2.2\publish\'
    ArtifactName: 'APPOINTMENTS-SCHEDULER-SERVICE-DROP'

- task: PublishBuildArtifacts@1
  displayName: 'Publish Artifact: HEALTH-CARDS-SERVICE-DROP'
  inputs:
    PathtoPublish: 'backend\health-cards\bin\Release\netcoreapp2.2\publish\'
    ArtifactName: 'HEALTH-CARDS-SERVICE-DROP'

```