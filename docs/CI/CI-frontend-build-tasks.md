# Fronted Build: Tasks

```yaml
resources:
- repo: self

pool:
  name: Hosted
steps:
- task: NodeTool@0
  displayName: 'Use Node 11.x'
  inputs:
    versionSpec: 11.x

- task: Npm@1
  displayName: 'npm install'
  inputs:
    workingDir: frontend
    verbose: false

- task: Npm@1
  displayName: 'npm build'
  inputs:
    command: custom
    workingDir: frontend
    verbose: false
    customCommand: 'run build'

- script: '.\node_modules\.bin\ng test --watch=false --reporters=junit,progress --browsers=ChromeHeadless'
  workingDirectory: frontend
  displayName: 'Run Unit Tests'

- task: PublishTestResults@2
  displayName: 'Publish Test Results **/TEST-*.xml'
  inputs:
    searchFolder: '$(System.DefaultWorkingDirectory)\frontend'

- task: PublishBuildArtifacts@1
  displayName: 'Publish Build Artifact: FE-DROP'
  inputs:
    PathtoPublish: frontend/dist
    ArtifactName: 'FE-DROP'


```