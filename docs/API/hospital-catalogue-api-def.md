# Hospital Catalogue API definition

```yaml
swagger: '2.0'
info:
  description: (Supporting) Hospital catalogue service for the managment of generic enetities
  version: 1.0.0
  title: Hospital catalogue service 
  # put the contact info for your development or API team
  contact:
    email: ppisecky@gmail.com

  license:
    name: Apache 2.0
    url: http://www.apache.org/licenses/LICENSE-2.0.html

# tags are used for organizing operations
tags:
- name: ambulances
  description: Catalogue of ambulances present in the hospital
- name: diseases
  description: Catalogue of diseases
- name: shifts
  description: Shift schedule
  
paths:
  /ambulances:
    get:
      tags: 
      - ambulances
      summary: Get ambulances
      description: Fetch a list of all ambulances in the hospital or search for specific ambulances identified by the provided list of ids.
      operationId: getAmbulances
      produces: 
      - application/json
      parameters:
      - in: query
        name: ids
        description: List of ambulance ids to look for.
        required: false
        type: array
        items: 
          type: integer
      responses:
        200:
          description: List of found ambulances.
          schema:
            type: 'array'
            items:
              $ref: '#/definitions/Ambulance'
          examples: 
            application/json:
            - id: 1
              name: Skolioticka ambulancia
              description: 
              room_identifier: '1.1'
            - id: 2
              name: Interná ambulancia
              description: 
              room_identifier: '1.2'
            - id: 3
              name: Geriatrická ambulancia
              description: 
              room_identifier: '2.3'
  /ambulances/{id}:
    get:
      tags: 
      - ambulances
      summary: Find an ambulance by its id
      description: Find the ambulance identified by the id provided in the path. 
      operationId: getAmbulance
      produces: 
      - application/json
      parameters: 
      - in: path 
        name: id
        description: ID of the ambulance
        required: true
        type: integer 
      responses:
        200:
          description: The ambulance.
          schema:
            $ref: '#/definitions/Ambulance'
          examples: 
            application/json:
            - id: 1
              name: Skolioticka ambulancia
              description: 
              room_identifier: '1.1'
        404:
          description: Ambulance not found.
  /diseases:
    get:
      tags: 
      - diseases
      summary: Search for diseases
      description: Search in the catalogue of diseases
      operationId: getDiseases
      produces: 
      - application/json
      parameters: 
      - in: query 
        name: q
        description: Query string used for finding diseases by name
        required: false
        type: string
      responses:
        200:
          description: List of found diseases.
          schema:
            type: 'array'
            items:
              $ref: '#/definitions/Disease'
          examples: 
            application/json:
            - id: 1
              name: Influenza
              severity: 3
            - id: 2
              name: Gastrovascular cancer
              severity: 10
            - id: 3
              name: Hickups
              severity: 1
  /diseases/{id}:
    get:
      tags: 
      - diseases
      summary: Find disease by id
      description: Find the disease identified by the id provided in the path. 
      operationId: getDisease
      produces: 
      - application/json
      parameters: 
      - in: path 
        name: id
        description: ID of the disease
        required: true
        type: integer 
      responses:
        200:
          description: The disease.
          schema:
            $ref: '#/definitions/Disease'
          examples: 
            application/json:
            - id: 1
              name: Influenza
              severity: 3
        404:
          description: Disease not found.
          
  /shifts/{ambulanceId}:
    get:
      tags: 
      - shifts
      summary: Find shift by ambulance_id and date
      description: Find the shift identified by the ambulance_id provided in the path and date provided in the query. 
      operationId: getAmbulanceShift
      produces: 
      - application/json
      parameters: 
      - in: path 
        name: ambulanceId
        description: ID of the ambulance
        required: true
        type: integer
      - in: query 
        name: date
        description: Date of the shift
        required: true
        type: string
        format: date
      responses:
        200:
          description: Shift details for the provided date.
          schema:
            $ref: '#/definitions/Shift'
          examples: 
            application/json:
             - id: 1 
               id_ambulance: 1 
               id_doctor: 1 
               date: '2018-07-01'
        404:
          description: No shift found for the given date.
          
definitions:
  Ambulance:
    type: object
    required:
    - id
    - name
    - description
    - room_identifier
    properties:
      id:
        type: integer
        format: int32
        example: 1
      name:
        type: string
        example: Pediatric ambulance
        description: Name of the ambulance
      room_identifier:
        type: string
        pattern: '^[0-9]{1,2}\.[0-9]{1,3}$'
        description: Ambulance door number in the form of [floor_number].[door_number]
        example: 1.15
      description:
        type: string
        example: Ambulance for the treatment of child diseases
        description: Specialization of the ambulance and other detailed information
  Disease:
    type: object
    required:
    - id
    - name
    - severity
    properties:
      id:
        type: integer
        format: int32
        example: 1
      name:
        type: string
        example: Influenza
        description: Commonly recognized medical name of the disease
      severity:
        type: integer
        minimum: 1
        maximum: 10
        example: 7
        description: Severity of the disease - <1, 10> where 1 is the lowest level of severity.
  Shift:
    type: object
    required: 
      - id
      - id_ambulance
      - id_doctor
      - date
    properties:
      id:
        type: integer
        format: int32
        example: 1
      id_ambulance:
        type: integer
        example: 1
        description: ID of the ambulance
      id_doctor:
        type: integer
        example: 1
        description: ID of the doctor
      date:
        type: string
        format: date
        example: '2018-07-01'
        description: Date of the scheduled shift 
# Added by API Auto Mocking Plugin
# host: wac-legends.com
# basePath: /hospital-catalogue-service
schemes:
 - https
# Added by API Auto Mocking Plugin
host: virtserver.swaggerhub.com
basePath: /wac-legends/hospital-catalogue-service/1.0.0

```