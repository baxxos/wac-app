# Appointments Scheduler API definition

```yaml
swagger: '2.0'
info:
  description: (Core) Service for scheduling ambulance appointments
  version: 1.0.0
  title: Appointment Scheduling Service
  # put the contact info for your development or API team
  contact:
    email: meluch.m@gmail.com

  license:
    name: Apache 2.0
    url: http://www.apache.org/licenses/LICENSE-2.0.html

# tags are used for organizing operations
tags:
- name: debug
  description: Development-only calls used for debugging
- name: appointments
  description: Management of ambulance appointments

paths:
  /:
    get:
      tags:
      - debug
      summary: Returns all currently stored appointments
      operationId: getAllAppointments
      description: Returns a list of all appointments currently stored in the DB
      produces:
      - text/plain
      - application/json
      responses:
        200:
          description: A list of all appointments is returned
          schema:
            type: array
            items:
              $ref: '#/definitions/Appointment'
  /seed:
    get:
      tags:
      - debug
      summary: Initializes database with fake appointment entries
      operationId: seedDB
      description: |
        Initializes the database with several fake appointment entries. These
        are hardcoded and their foreign keys are most likely incorrect.
      produces:
      - text/plain
      - application/json
      responses:
        200:
          description: Successful database initialization
        
  /{appointmentId}:
    delete:
      tags: 
      - appointments
      summary: Deletes an existing appointment
      operationId: delAppointmentById
      description: Search for the appointment identified by its ID and delete it
      parameters: 
      - in: path
        name: appointmentId
        description: Identifier
        required: true
        type: integer
      responses:
        204:
          description: Appointment was successfully deleted
        404:
          description: Specified appointment was not found
          
  /ambulance/{ambulanceId}:
    get:
      tags:
      - appointments
      summary: Searches the appointments list
      operationId: searchAppointments
      description: |
        By passing in the appropriate ambulance identifier, you can search for
        appointment(s) scheduled for a specified timeslot of a day. 
      produces:
      - application/json
      parameters:
      - in: path
        name: ambulanceId
        description: Identifier required for determining the ambulance
        required: true
        type: integer
      - in: query
        name: dateFrom
        description: Start time of the appointment(s)
        required: true
        type: string
        format: date-time
      - in: query
        name: dateTo
        description: End time of the appointment(s)
        required: true
        type: string
        format: date-time
      - in: query
        name: patientId
        description: Patient identifier
        required: false
        type: integer
      responses:
        200:
          description: Details of the appointment matching the specified ambulance and timeslot
          schema:
            type: array
            items:
              $ref: '#/definitions/Appointment'
          examples: 
            application/json:
              - id: 123
                ambulanceId: 45
                patientId: 678
                diseaseId: 3
                timeStart: 2018-12-24T10:00:00Z
                timeEnd: 2018-12-24T11:00:00Z
                notes: Flu lasting for 3 weeks already
        400:
          description: Bad input parameter(s)
        404:
          description: No appointment matching the specified ambulance and timeslot was found

    post:
      tags:
      - appointments
      summary: Adds a new appointment
      operationId: addAppointment
      description: |
        By passing in the appropriate ambulance identifier, you can create a new
        appointment instance scheduled for the specified timeslot of a day.
      produces:
      - application/json
      - string
      consumes:
      - application/json
      parameters: 
      - in: path
        name: ambulanceId
        description: Identifier required for determining the related ambulance
        required: true
        type: integer
      - in: body
        name: appointmentDetails
        description: The newly planned appointment object
        required: true
        schema:
          $ref: '#/definitions/Appointment'
      responses:
        201:
          description: Newly created appointment object with an auto-generated ID
          schema:
            $ref: '#/definitions/Appointment'
        400:
          description: Malformed input data
    delete:
      tags: 
      - appointments
      summary: Deletes an existing appointment
      operationId: delAppointmentByDate
      description: Search for the appointment identified by ambulance ID and time and delete it
      parameters: 
      - in: path
        name: ambulanceId
        description: Identifier required for determining the related ambulance
        required: true
        type: integer
      - in: query
        name: dateFrom
        description: Start time of the appointment(s) to be deleted
        required: true
        type: string
        format: date-time
      - in: query
        name: dateTo
        description: End time of the appointment(s) to be deleted
        required: true
        type: string
        format: date-time
      responses:
        204:
          description: Appointment was successfully deleted
        404:
          description: Specified appointment was not found
          
definitions:
  Appointment:
    description: Describes a doctor's appointment in a specified ambulance
    type: object
    required:
      - ambulanceId
      - patientId
      - diseaseId
      - timeStart
      - timeEnd
    properties:
      id:
        type: integer
        example: 123
      ambulanceId:
        type: integer
        example: 45
      patientId:
        type: integer
        example: 678
      diseaseId:
        type: integer
        example: 3
      timeStart:
        type: string
        format: date-time
        example: 2016-08-29T09:12:30:000Z
      timeEnd:
        type: string
        format: date-time
        example: 2016-08-29T09:12:30:000Z
      notes:
        type: string
        example: Flu lasting for 3 weeks already
# Added by API Auto Mocking Plugin
host: virtserver.swaggerhub.com
# basePath: /appointments
schemes:
 - https
# Added by API Auto Mocking Plugin
# basePath: /appointments
# Added by API Auto Mocking Plugin
# basePath: /appointments
# Added by API Auto Mocking Plugin
basePath: /baxxos/WAC-appointments-scheduler/1.0.0
```